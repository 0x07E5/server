package service

import (
	"errors"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	"golang.org/x/exp/slices"
)

var ErrGachaServiceUserNotFound = errors.New("user not found")
var ErrGachaServiceGachaNotAvailable = errors.New("gacha is not available")
var ErrGachaServiceServerExplode = errors.New("server exploded")
var ErrGachaServiceUserItemShort = errors.New("user item is short")
var ErrGachaServiceUserGemShort = errors.New("user gem is short")
var ErrGachaServiceUserUnlimitedGemShort = errors.New("user unlimited gem is short")

// TODO: Rename model name (ManagedCharacter -> UserManagedCharacter)
type UserGachaService interface {
	DrawGacha(userId uint, gachaId uint, param value.GachaDrawParam) (*model_user.User, []value.GachaResult, error)
	RefreshUserGacha(userId uint) (*model_user.User, error)
	refreshUserGachaByModel(user *model_user.User) (*model_user.User, error)
}

type userGachaService struct {
	uu usecase.UserUsecase
	gu usecase.GachaUsecase
	cu usecase.CharacterUsecase
	iu usecase.ItemUsecase
	nu usecase.NamedTypeUsecase
}

func NewUserGachaService(
	uu usecase.UserUsecase,
	gu usecase.GachaUsecase,
	iu usecase.ItemUsecase,
	cu usecase.CharacterUsecase,
	nu usecase.NamedTypeUsecase,
) UserGachaService {
	return &userGachaService{uu, gu, cu, iu, nu}
}

func (s *userGachaService) refreshUserGachaByModel(user *model_user.User) (*model_user.User, error) {
	// Get available gachas
	availableGachas, err := s.gu.GetAvailableGachas()
	if err != nil {
		return nil, ErrGachaServiceServerExplode
	}
	// Create availableGachaIds
	var availableGachaIds []uint
	for _, gacha := range availableGachas {
		availableGachaIds = append(availableGachaIds, gacha.GachaId)
	}

	// Recreate gachas and remove gacha from user if needed
	var newUserGachas []model_user.UserGacha
	var newUserGachaIds []uint
	for _, userGacha := range user.Gachas {
		if !slices.Contains(availableGachaIds, userGacha.GachaId) {
			continue
		}
		if userGacha.Gacha.IsUnconditional() && userGacha.Gacha.GachaId != 1 {
			newUserGachas = append(newUserGachas, userGacha)
		} else {
			if user.HasItemMoreThan(uint32(userGacha.Gacha.ItemId), uint32(userGacha.Gacha.ItemAmount)) {
				newUserGachas = append(newUserGachas, userGacha)
			}
		}
		newUserGachaIds = append(newUserGachaIds, userGacha.GachaId)
	}

	// Add gachas to user if needed
	for _, gacha := range availableGachas {
		if slices.Contains(newUserGachaIds, gacha.GachaId) {
			continue
		}
		if gacha.IsUnconditional() {
			newUserGachas = append(newUserGachas, model_user.NewUserGacha(user.Id, gacha.GachaId))
		} else {
			if user.HasItemMoreThan(uint32(gacha.ItemId), uint32(gacha.ItemAmount)) {
				newUserGachas = append(newUserGachas, model_user.NewUserGacha(user.Id, gacha.GachaId))
			}
		}
	}

	user.Gachas = newUserGachas

	return user, nil
}

func (s *userGachaService) RefreshUserGacha(userId uint) (*model_user.User, error) {
	// Check the user is available or not
	user, err := s.uu.GetUserByInternalId(userId, repository.UserRepositoryParam{Gachas: true})
	if err != nil {
		return nil, ErrGachaServiceUserNotFound
	}
	user, err = s.refreshUserGachaByModel(user)
	if err != nil {
		return nil, ErrGachaServiceServerExplode
	}
	return user, nil
}

func (s *userGachaService) DrawGacha(userId uint, gachaId uint, param value.GachaDrawParam) (*model_user.User, []value.GachaResult, error) {
	// Check the user is available or not
	user, err := s.uu.GetUserByInternalId(userId, repository.UserRepositoryParam{
		Gachas:            true,
		ManagedCharacters: true,
		ManagedNamedTypes: true,
		ItemSummary:       true,
	})
	if err != nil {
		return nil, nil, ErrGachaServiceUserNotFound
	}
	// Check the gacha is available or not
	gacha, err := s.gu.GetSpecificGacha(gachaId)
	if err != nil || gacha == nil {
		return nil, nil, ErrGachaServiceGachaNotAvailable
	}

	// Consume item or gem
	switch param.DrawType {
	case value.GachaDrawTypeUnlimitedGem1:
		if consumed := user.ConsumeUnlimitedGem(uint32(gacha.UnlimitedGem)); !consumed {
			return nil, nil, ErrGachaServiceUserUnlimitedGemShort
		}
	case value.GachaDrawTypeGem1:
		if consumed := user.ConsumeLimitedGem(uint32(gacha.Gem1)); !consumed {
			return nil, nil, ErrGachaServiceUserGemShort
		}
	case value.GachaDrawTypeGem10:
		if consumed := user.ConsumeLimitedGem(uint32(gacha.Gem10)); !consumed {
			return nil, nil, ErrGachaServiceUserGemShort
		}
	case value.GachaDrawTypeItem:
		if gacha.ItemAmount != -1 {
			consumeCount := uint32(gacha.ItemAmount)
			if param.Is10Roll {
				consumeCount *= 10
			}
			if consumed := user.ConsumeItem(uint32(gacha.ItemId), consumeCount); !consumed {
				return nil, nil, ErrGachaServiceUserItemShort
			}
		}
	}

	// Draw the gacha
	characterIds, err := s.gu.DrawGacha(gachaId, param)
	if err != nil || len(characterIds) == 0 {
		return nil, nil, ErrGachaServiceServerExplode
	}

	// Update roll count / step of gacha
	for _, gacha := range user.Gachas {
		if gacha.GachaId != gachaId {
			continue
		}
		switch param.DrawType {
		case value.GachaDrawTypeUnlimitedGem1:
			gacha.UGem1Daily++
			gacha.UGem1Total++
		case value.GachaDrawTypeGem1:
			gacha.Gem1Daily++
			gacha.Gem1Total++
		case value.GachaDrawTypeGem10:
			gacha.Gem10CurrentStep++
			gacha.Gem10Daily++
			gacha.Gem10Total++
		case value.GachaDrawTypeItem:
			if param.Is10Roll {
				gacha.ItemDaily += 10
				gacha.ItemTotal += 10
			} else {
				gacha.ItemDaily++
				gacha.ItemTotal++
			}
		}
		break
	}

	// Create Gacha result object
	gachaResults := []value.GachaResult{}

	// Updates userManagedCharacters and userManagedNamedTypes field
	for _, characterId := range characterIds {
		duplicated := user.AddCharacter(characterId)
		character, err := s.cu.GetCharacterById(characterId)
		if err != nil {
			return nil, nil, ErrPresentServiceServerExplode
		}
		if duplicated {
			user.AddItem(character.AltItemID, 1)
			gachaResults = append(gachaResults, value.GachaResult{
				CharacterId: characterId,
				ItemId:      int32(character.AltItemID),
				ItemAmount:  1,
			})
		} else {
			gachaResults = append(gachaResults, value.GachaResult{
				CharacterId: characterId,
				ItemId:      -1,
				ItemAmount:  0,
			})
		}
		namedType, err := s.nu.GetNamedTypeById(uint(character.NamedType))
		if err != nil {
			return nil, nil, ErrPresentServiceServerExplode
		}
		// NOTE: Ignore the duplicated error
		user.AddNamedType(uint16(namedType.NamedType), namedType.TitleType)
	}

	// Updates userGachas field
	user, err = s.refreshUserGachaByModel(user)
	if err != nil {
		return nil, nil, ErrGachaServiceServerExplode
	}

	// Save the user model changes
	_, err = s.uu.UpdateUser(user, repository.UserRepositoryParam{
		Gachas:            true,
		ManagedCharacters: true,
		ManagedNamedTypes: true,
		ItemSummary:       true,
	})
	if err != nil {
		return nil, nil, ErrGachaServiceServerExplode
	}

	if user.StepCode == value.StepCodeAdvCreaDone {
		user, err = s.uu.SetupUser(userId)
		if err != nil {
			return nil, nil, ErrGachaServiceServerExplode
		}
	}

	// TODO: Make a better response type, model_user.ManagedCharacter is too big
	return user, gachaResults, nil
}
