package service

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/migrate"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/database/seed"
)

func Test_userQuestService_StartQuest(t *testing.T) {
	logger := database.InitLogger()
	logRepo := database.InitLoggerRepo(logger)
	db := database.InitDatabase(logRepo, logger)
	migrate.AutoMigrate(db)
	seed.AutoSeed(db)

	uu := InitializeUserUsecase(logRepo, db)
	s := InitializeUserQuestService(logRepo, db)

	type args struct {
		userId               uint
		questId              uint
		managedBattlePartyId uint
		supportCharacterId   int64
		questNpcId           int64
	}
	tests := []struct {
		name    string
		args    args
		want    *model_user.User
		want1   *[][]*model_quest.QuestWave
		want2   *[][][]model_quest.QuestWaveDrop
		wantErr bool
	}{
		{
			name: "start a quest success",
			args: args{
				userId:               2,
				questId:              1100010,
				managedBattlePartyId: 16,
				supportCharacterId:   1,
				questNpcId:           1,
			},
			want1:   nil,
			want2:   nil,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u, err := uu.CreateUser("test", "test", nil, nil, nil)
			if err != nil {
				t.Errorf("userRepo.CreateUser error = %v, wantErr nil", err)
			}
			got, got1, got2, err := s.StartQuest(
				u.Id,
				tt.args.questId,
				tt.args.managedBattlePartyId,
				tt.args.supportCharacterId,
				tt.args.questNpcId,
			)
			t.Logf("got: %+v", got)
			t.Logf("got1: %+v", got1)
			t.Logf("got2: %+v", got2)
			if (err != nil) != tt.wantErr {
				t.Errorf("userQuestService.StartQuest() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func Test_userQuestService_CompleteQuest(t *testing.T) {
	logger := database.InitLogger()
	logRepo := database.InitLoggerRepo(logger)
	db := database.InitDatabase(logRepo, logger)
	migrate.AutoMigrate(db)
	seed.AutoSeed(db)

	uu := InitializeUserUsecase(logRepo, db)
	s := InitializeUserQuestService(logRepo, db)

	type args struct {
		state             uint8
		clearRank         value.ClearRank
		skillExps         string
		weaponSkillExps   string
		friendUseNum      uint8
		masterSkillUseNum uint8
		uniqueSkillUseNum uint8
		stepCode          value.StepCode
	}

	// TODO: Fix the failing test cases
	tests := []struct {
		name    string
		args    args
		want    *model_user.User
		wantErr bool
	}{
		{
			name: "complete a quest success",
			args: args{
				state:             2,
				clearRank:         value.ClearRankGold,
				skillExps:         "",
				weaponSkillExps:   "",
				friendUseNum:      0,
				masterSkillUseNum: 0,
				uniqueSkillUseNum: 0,
				stepCode:          value.StepCodeFirstQuestStart,
			},
			want: &model_user.User{
				Gold:                 1200,
				Level:                0,
				LevelExp:             10,
				UnlimitedGem:         0,
				LimitedGem:           5,
				MyCode:               "",
				Name:                 "",
				TotalExp:             10,
				State:                0,
				ItemSummary:          []model_user.ItemSummary{},
				ManagedBattleParties: []model_user.ManagedBattleParty{},
				ManagedCharacters:    []model_user.ManagedCharacter{},
				ManagedNamedTypes:    []model_user.ManagedNamedType{},
				FlagUi:               0,
				FlagPush:             0,
				FlagStamina:          0,
				IsNewProduct:         0,
				IsCloseInfo:          0,
				TrainingCount:        0,
				PresentCount:         0,
				FriendProposedCount:  0,
				NewAchievementCount:  0,
				StepCode:             value.StepCodeFirstQuestDone,
				LatestQuestLogID:     -1,
				QuestLogs:            []model_user.QuestLog{},
				QuestAccesses:        []model_user.QuestAccess{},
			},
			wantErr: false,
		},
	}

	ignoreFields := []string{
		"Id",
		"CreatedAt",
		"UpdatedAt",
		"UUId",
		"Session",
		"ContinuousDays",
		"FacilityLimit",
		"FriendLimit",
		"IpAddr",
		"KiraraLimit",
		"LastLoginAt",
		"Level",
		"MyCode",
		"Name",
		"PartyCost",
		"RecastTimeMax",
		"RoomObjectLimit",
		"Stamina",
		"StaminaMax",
		"StaminaUpdatedAt",
		"State",
		"SupportLimit",
		"UserAgent",
		"WeaponLimit",
		"SupportCharacters",
		"ManagedBattleParties",
		"ManagedFacilities",
		"ManagedFieldPartyMembers",
		"ManagedRoomObjects",
		"ManagedRooms",
		"ManagedTowns",
		"ManagedMasterOrbs",
		"ManagedAbilityBoards",
		"FavoriteMembers",
		"OfferTitleTypes",
		"AdvIds",
		"TipIds",
		"FlagUi",
		"FlagPush",
		"FlagStamina",
		"IsCloseInfo",
		"PresentCount",
		"LastMemberAdded",
		"LastOpenedPart1ChapterId",
		"LastOpenedPart2ChapterId",
		"LastPlayedPart1ChapterQuestId",
		"LastPlayedPart2ChapterQuestId",
		"QuestLogs",
		"QuestAccesses",
		"Missions",
		"Presents",
		"Gachas",
	}
	opts := []cmp.Option{
		cmpopts.IgnoreFields(model_user.User{}, ignoreFields...),
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u, err := uu.CreateUser("test", "test", nil, nil, nil)
			if err != nil {
				t.Errorf("userRepo.CreateUser error = %v, wantErr nil", err)
			}

			u, _, _, err = s.StartQuest(
				u.Id,
				1100010,
				16,
				1,
				1,
			)
			if err != nil {
				t.Errorf("userQuestService.StartQuest() error = %v, wantErr nil", err)
				return
			}

			got, _, err := s.CompleteQuest(u.Id, uint(u.LatestQuestLogID), tt.args.state, tt.args.clearRank, tt.args.skillExps, tt.args.weaponSkillExps, tt.args.friendUseNum, tt.args.masterSkillUseNum, tt.args.uniqueSkillUseNum, tt.args.stepCode)
			if (err != nil) != tt.wantErr {
				t.Errorf("userQuestService.CompleteQuest() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if cmp.Equal(*got, *tt.want, opts...) != true {
				t.Errorf("userQuestService.CompleteQuest() Diff = %+v", cmp.Diff(*got, *tt.want, opts...))
				return
			}
		})
	}
}

func Test_userQuestService_SaveQuest(t *testing.T) {
	logger := database.InitLogger()
	logRepo := database.InitLoggerRepo(logger)
	db := database.InitDatabase(logRepo, logger)
	migrate.AutoMigrate(db)
	seed.AutoSeed(db)

	uu := InitializeUserUsecase(logRepo, db)
	s := InitializeUserQuestService(logRepo, db)

	type args struct {
		questData string
	}

	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "save a quest progress success",
			args: args{
				questData: "test",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u, err := uu.CreateUser("test", "test", nil, nil, nil)
			if err != nil {
				t.Errorf("userRepo.CreateUser error = %v, wantErr nil", err)
			}

			u, _, _, err = s.StartQuest(
				u.Id,
				1100010,
				16,
				1,
				1,
			)
			if err != nil {
				t.Errorf("userQuestService.StartQuest() error = %v, wantErr nil", err)
				return
			}

			if err := s.SaveQuest(u.Id, u.LatestQuestLogID, tt.args.questData); (err != nil) != tt.wantErr {
				t.Errorf("userQuestService.SaveQuest() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
