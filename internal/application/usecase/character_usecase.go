package usecase

import (
	model_character "gitlab.com/kirafan/sparkle/server/internal/domain/model/character"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type CharacterUsecase interface {
	GetCharacterById(characterId value.CharacterId) (*model_character.Character, error)
}

type characterUsecase struct {
	rp     repository.CharacterRepository
	logger repository.LoggerRepository
}

func NewCharacterUsecase(rp repository.CharacterRepository, logger repository.LoggerRepository) CharacterUsecase {
	return &characterUsecase{rp, logger}
}

func (uc *characterUsecase) GetCharacterById(characterId value.CharacterId) (*model_character.Character, error) {
	character, err := uc.rp.FindCharacter(&model_character.Character{CharacterId: uint64(characterId)}, nil, nil)
	if err != nil {
		return nil, err
	}
	return character, nil
}
