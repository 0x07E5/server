package usecase

import (
	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type MissionUsecase interface {
	GetTutorialMissions() ([]*model_mission.Mission, error)
	GetDailyMissions() ([]*model_mission.Mission, error)
	GetWeeklyMissions() ([]*model_mission.Mission, error)
	GetEventMissions() ([]*model_mission.Mission, error)
	GetNextRankUpMission(currentCount uint) (*model_mission.Mission, error)
	GetNextCharacterRelationshipMission(currentCount uint) (*model_mission.Mission, error)
	GetNextCharacterEvolutionMission(currentCount uint) (*model_mission.Mission, error)
}

type missionUsecase struct {
	mr     repository.MissionRepository
	logger repository.LoggerRepository
}

func NewMissionUsecase(mr repository.MissionRepository, logger repository.LoggerRepository) MissionUsecase {
	return &missionUsecase{mr, logger}
}

func (mu *missionUsecase) GetTutorialMissions() ([]*model_mission.Mission, error) {
	criteria := map[string]interface{}{
		"mission_insert_type": value.MissionInsertTypeTutorial,
	}
	missions, err := mu.mr.FindMissions(nil, criteria, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return missions, nil
}

func (mu *missionUsecase) GetDailyMissions() ([]*model_mission.Mission, error) {
	missions, err := mu.mr.FindMissions(&model_mission.Mission{
		MissionInsertType: value.MissionInsertTypeDaily,
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return missions, nil
}

func (mu *missionUsecase) GetWeeklyMissions() ([]*model_mission.Mission, error) {
	missions, err := mu.mr.FindMissions(&model_mission.Mission{
		MissionInsertType: value.MissionInsertTypeWeekly,
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return missions, nil
}

func (mu *missionUsecase) GetEventMissions() ([]*model_mission.Mission, error) {
	missions, err := mu.mr.FindMissions(&model_mission.Mission{
		MissionInsertType: value.MissionInsertTypeEvent,
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return missions, nil
}

func (mu *missionUsecase) GetNextRankUpMission(currentCount uint) (*model_mission.Mission, error) {
	mission, err := mu.mr.FindMission(&model_mission.Mission{
		MissionInsertType: value.MissionInsertTypeRankUp,
		Rate:              uint32(currentCount),
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return mission, nil
}

func (mu *missionUsecase) GetNextCharacterRelationshipMission(currentCount uint) (*model_mission.Mission, error) {
	mission, err := mu.mr.FindMission(&model_mission.Mission{
		MissionInsertType: value.MissionInsertTypeCharacterRelationship,
		Rate:              uint32(currentCount),
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return mission, nil
}

func (mu *missionUsecase) GetNextCharacterEvolutionMission(currentCount uint) (*model_mission.Mission, error) {
	mission, err := mu.mr.FindMission(&model_mission.Mission{
		MissionInsertType: value.MissionInsertTypeCharacterEvolution,
		Rate:              uint32(currentCount),
	}, nil, []string{"Reward"})
	if err != nil {
		return nil, err
	}
	return mission, nil
}
