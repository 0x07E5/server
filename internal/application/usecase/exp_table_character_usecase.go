package usecase

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type ExpTableCharacterUsecase interface {
	GetNextExpTableCharacter(currentExp uint64) (*model_exp_table.ExpTableCharacter, error)
}

type expTableCharacterUsecase struct {
	rp     repository.ExpTableCharacterRepository
	logger repository.LoggerRepository
}

func NewExpTableCharacterUsecase(rp repository.ExpTableCharacterRepository, logger repository.LoggerRepository) ExpTableCharacterUsecase {
	return &expTableCharacterUsecase{rp, logger}
}

func (uc *expTableCharacterUsecase) GetNextExpTableCharacter(currentExp uint64) (*model_exp_table.ExpTableCharacter, error) {
	criteria := map[string]interface{}{
		"total_exp >= ?": currentExp,
	}
	expTableRank, err := uc.rp.FindExpTableCharacter(nil, criteria)
	if err != nil {
		return nil, err
	}
	return expTableRank, nil
}
