package usecase

import (
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/go-cmp/cmp/cmpopts"
	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func Test_missionUsecase_GetTutorialMissions(t *testing.T) {
	missionRepository := persistence.NewMissionRepositoryImpl(db)
	missionUsecase := NewMissionUsecase(missionRepository, logRepo)

	tests := []struct {
		name         string
		err          error
		expectIndex0 model_mission.Mission
	}{
		{
			name: "GetTutorialMissions success",
			err:  nil,
			expectIndex0: model_mission.Mission{
				MissionId:         1057,
				Category:          value.MissionCategoryWeekly,
				MissionInsertType: value.MissionInsertTypeTutorial,
				TargetMessage:     "クエストを{0}回クリア",
				MissionSegType:    value.MissionSegTypeQuestClear,
				MissionFuncType:   value.MissionFuncTypeQuestClearCount,
				SubCode:           0,
				Rate:              0,
				RateMax:           30,
				TransitParam:      0,
				TransitScene:      0,
				UiPriority:        0,
				Reward: []model_mission.MissionReward{
					{
						RewardType: value.MissionRewardTypeItem,
						RewardNo:   1009,
						RewardNum:  1,
					},
				},
			},
		},
	}

	opts := []cmp.Option{
		cmpopts.IgnoreFields(model_mission.MissionReward{}, "Id", "MissionId"),
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := missionUsecase.GetTutorialMissions()
			if err != nil {
				t.Errorf("missionUsecase.GetTutorialMissions() error = %v, wantErr nil", err)
				return
			}
			if cmp.Equal(&tt.expectIndex0, got[0], opts...) != true {
				t.Errorf("missionUsecase.GetTutorialMissions() Diff = %+v", cmp.Diff(tt.expectIndex0, got[0]))
				return
			}
		})
	}
}

func Test_missionUsecase_GetDailyMissions(t *testing.T) {
	missionRepository := persistence.NewMissionRepositoryImpl(db)
	missionUsecase := NewMissionUsecase(missionRepository, logRepo)

	tests := []struct {
		name         string
		err          error
		expectIndex0 model_mission.Mission
	}{
		{
			name: "GetDailyMissions success",
			err:  nil,
			expectIndex0: model_mission.Mission{
				MissionId:         21,
				Category:          value.MissionCategoryDaily,
				MissionInsertType: value.MissionInsertTypeDaily,
				TargetMessage:     "トレーニングに{0}回出発する",
				MissionSegType:    value.MissionSegTypeTraining,
				MissionFuncType:   value.MissionFuncTypeTrainingStartCount,
				SubCode:           0,
				Rate:              0,
				RateMax:           1,
				TransitParam:      0,
				TransitScene:      0,
				UiPriority:        0,
				Reward: []model_mission.MissionReward{
					{
						RewardType: value.MissionRewardTypeItem,
						RewardNo:   1003,
						RewardNum:  5,
					},
				},
			},
		},
	}

	opts := []cmp.Option{
		cmpopts.IgnoreFields(model_mission.Mission{}, "Reward"),
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := missionUsecase.GetDailyMissions()
			if err != nil {
				t.Errorf("missionUsecase.GetDailyMissions() error = %v, wantErr nil", err)
				return
			}
			if cmp.Equal(&tt.expectIndex0, got[0], opts...) != true {
				t.Errorf("missionUsecase.GetDailyMissions() Diff = %+v", cmp.Diff(tt.expectIndex0, got[0]))
				return
			}
		})
	}
}

func Test_missionUsecase_GetWeeklyMissions(t *testing.T) {
	missionRepository := persistence.NewMissionRepositoryImpl(db)
	missionUsecase := NewMissionUsecase(missionRepository, logRepo)

	tests := []struct {
		name         string
		err          error
		expectIndex0 model_mission.Mission
	}{
		{
			name: "GetWeeklyMissions success",
			err:  nil,
			expectIndex0: model_mission.Mission{
				MissionId:         1050,
				Category:          value.MissionCategoryWeekly,
				MissionInsertType: value.MissionInsertTypeWeekly,
				TargetMessage:     "5日間連続でログインする",
				MissionSegType:    value.MissionSegTypeServer,
				MissionFuncType:   value.MissionFuncTypeServerLoginContinuousCount,
				SubCode:           0,
				Rate:              0,
				RateMax:           5,
				TransitParam:      0,
				TransitScene:      0,
				UiPriority:        0,
				Reward: []model_mission.MissionReward{
					{
						RewardType: value.MissionRewardTypeItem,
						RewardNo:   1003,
						RewardNum:  5,
					},
				},
			},
		},
	}

	opts := []cmp.Option{
		cmpopts.IgnoreFields(model_mission.Mission{}, "Reward"),
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := missionUsecase.GetWeeklyMissions()
			if err != nil {
				t.Errorf("missionUsecase.GetWeeklyMissions() error = %v, wantErr nil", err)
				return
			}
			if cmp.Equal(&tt.expectIndex0, got[0], opts...) != true {
				t.Errorf("missionUsecase.GetWeeklyMissions() Diff = %+v", cmp.Diff(tt.expectIndex0, got[0]))
				return
			}
		})
	}
}
