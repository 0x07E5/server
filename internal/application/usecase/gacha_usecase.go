package usecase

import (
	"errors"
	"time"

	model_gacha "gitlab.com/kirafan/sparkle/server/internal/domain/model/gacha"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type GachaUsecase interface {
	GetAvailableGachas() ([]*model_gacha.Gacha, error)
	GetInitialGacha() (*model_gacha.Gacha, error)
	GetSpecificGacha(gachaId uint) (*model_gacha.Gacha, error)
	IsGachaAvailable(gachaId uint) (bool, error)
	DrawGacha(gachaId uint, param value.GachaDrawParam) ([]value.CharacterId, error)
}

type gachaUsecase struct {
	rp     repository.GachaRepository
	logger repository.LoggerRepository
}

func NewGachaUsecase(rp repository.GachaRepository, logger repository.LoggerRepository) GachaUsecase {
	return &gachaUsecase{rp, logger}
}

func (uc *gachaUsecase) GetAvailableGachas() ([]*model_gacha.Gacha, error) {
	now := time.Now()
	criteria := map[string]interface{}{
		"type = ?":           value.GachaTypeNormal,
		"disp_start_at <= ?": now,
		"disp_end_at >= ?":   now,
	}
	gachas, err := uc.rp.FindGachas(nil, criteria, nil)
	if err != nil {
		return nil, err
	}
	return gachas, nil
}

func (uc *gachaUsecase) GetInitialGacha() (*model_gacha.Gacha, error) {
	gacha, err := uc.rp.FindGacha(&model_gacha.Gacha{GachaId: 1}, nil, nil)
	if err != nil {
		return nil, err
	}
	return gacha, nil
}

func (uc *gachaUsecase) GetSpecificGacha(gachaId uint) (*model_gacha.Gacha, error) {
	criteria := map[string]interface{}{
		"gacha_id": gachaId,
	}
	gacha, err := uc.rp.FindGacha(nil, criteria, nil)
	if err != nil {
		return nil, err
	}
	return gacha, nil
}

func (uc *gachaUsecase) IsGachaAvailable(gachaId uint) (bool, error) {
	_, err := uc.GetSpecificGacha(gachaId)
	if err != nil {
		return false, err
	}
	return true, nil
}

func (uc *gachaUsecase) DrawGacha(gachaId uint, param value.GachaDrawParam) ([]value.CharacterId, error) {
	gacha, err := uc.GetSpecificGacha(gachaId)
	if err != nil {
		return nil, err
	}
	var characterIds []value.CharacterId
	// TODO: Implement normal gacha
	switch gacha.Type {
	case value.GachaTypeSelectable:
		characterId, err := value.NewCharacterId(uint32(param.SelectedCharacterId))
		if err != nil {
			return nil, err
		}
		characterIds = append(characterIds, characterId)
	default:
		return nil, errors.New("not implemented gacha type")
	}
	return characterIds, nil
}
