package usecase

import (
	"errors"
	"fmt"

	model_version "gitlab.com/kirafan/sparkle/server/internal/domain/model/version"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type VersionUsecase interface {
	FindByPlatformAndVersion(platform uint16, version string) (*model_version.Version, error)
}

type versionUsecase struct {
	versionRepo repository.VersionRepository
	logger      repository.LoggerRepository
}

func NewVersionUsecase(versionRepo repository.VersionRepository, logger repository.LoggerRepository) VersionUsecase {
	return &versionUsecase{versionRepo: versionRepo, logger: logger}
}

func (vu *versionUsecase) FindByPlatformAndVersion(platform uint16, version string) (*model_version.Version, error) {
	if platform > 2 {
		return nil, errors.New("platform not supported")
	}
	var p value.Platform
	switch platform {
	case 1:
		p = value.PlatformIOS
	case 2:
		p = value.PlatformAndroid
	}
	existedVersion, err := vu.versionRepo.FindByPlatformAndVersion(p, version)
	vu.logger.Debug(fmt.Sprintf("Existed version: %+v", existedVersion))
	if err != nil {
		return nil, errors.New("version not supported")
	}
	return existedVersion, nil
}
