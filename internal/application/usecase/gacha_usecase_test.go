package usecase

import (
	"testing"

	"gitlab.com/kirafan/sparkle/server/internal/infrastructure/persistence"
)

func Test_gachaUsecase_GetAll(t *testing.T) {
	gachaRepository := persistence.NewGachaRepositoryImpl(db)
	gachaUsecase := NewGachaUsecase(gachaRepository, logRepo)

	got, err := gachaUsecase.GetAvailableGachas()
	if err != nil {
		t.Errorf("gachaUsecase.GetGachaById(gachaId) error = %v, wantErr nil", err)
		return
	}
	t.Logf("gachaUsecase.GetGachaById(gachaId) got = %v", got)
}
