package usecase

import (
	model_item "gitlab.com/kirafan/sparkle/server/internal/domain/model/item"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type ItemUsecase interface {
	GetAllItems() ([]*model_item.Item, error)
	GetItemsByCategory(category value.ItemCategory) ([]*model_item.Item, error)
}

type itemUsecase struct {
	rp     repository.ItemRepository
	logger repository.LoggerRepository
}

func NewItemUsecase(rp repository.ItemRepository, logger repository.LoggerRepository) ItemUsecase {
	return &itemUsecase{rp, logger}
}

func (uc *itemUsecase) GetAllItems() ([]*model_item.Item, error) {
	items, err := uc.rp.FindItems(nil, nil, nil)
	if err != nil {
		return nil, err
	}
	return items, nil
}

func (uc *itemUsecase) GetItemsByCategory(category value.ItemCategory) ([]*model_item.Item, error) {
	items, err := uc.rp.FindItems(&model_item.Item{Category: category}, nil, nil)
	if err != nil {
		return nil, err
	}
	return items, nil
}
