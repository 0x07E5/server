package usecase

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

type ExpTableFriendshipUsecase interface {
	GetNextExpTableFriendship(currentExp uint64) (*model_exp_table.ExpTableFriendship, error)
}

type expTableFriendshipUsecase struct {
	rp     repository.ExpTableFriendshipRepository
	logger repository.LoggerRepository
}

func NewExpTableFriendshipUsecase(rp repository.ExpTableFriendshipRepository, logger repository.LoggerRepository) ExpTableFriendshipUsecase {
	return &expTableFriendshipUsecase{rp, logger}
}

func (uc *expTableFriendshipUsecase) GetNextExpTableFriendship(currentExp uint64) (*model_exp_table.ExpTableFriendship, error) {
	criteria := map[string]interface{}{
		"total_exp >= ?": currentExp,
	}
	expTableRank, err := uc.rp.FindExpTableFriendship(nil, criteria)
	if err != nil {
		return nil, err
	}
	return expTableRank, nil
}
