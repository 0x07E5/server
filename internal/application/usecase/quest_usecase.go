package usecase

import (
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type QuestUsecase interface {
	GetAll(internalUserId uint) (*model_quest.AllQuestInfo, error)
	GetQuest(questId uint) (*model_quest.Quest, error)
	GetPart1Quests(internalUserId uint) ([]*model_quest.Quest, error)
	GetPart2Quests(internalUserId uint) ([]*model_quest.Quest, error)
	GetEventQuests(internalUserId uint) ([]*model_quest.EventQuest, error)
	GetCharacterQuests(internalUserId uint) ([]*model_quest.CharacterQuest, error)
	GetCraftQuests(internalUserId uint) ([]*interface{}, error)
	GetPlayerOfferQuests(internalUserId uint) ([]*interface{}, error)
}

type questUsecase struct {
	qr     repository.QuestRepository
	logger repository.LoggerRepository
}

func NewQuestUsecase(qr repository.QuestRepository, logger repository.LoggerRepository) QuestUsecase {
	return &questUsecase{qr: qr, logger: logger}
}

func (uc *questUsecase) GetAll(internalUserId uint) (*model_quest.AllQuestInfo, error) {
	questPart1s, err := uc.GetPart1Quests(internalUserId)
	if err != nil {
		return nil, err
	}
	questPart2s, err := uc.GetPart2Quests(internalUserId)
	if err != nil {
		return nil, err
	}
	questEvents, err := uc.GetEventQuests(internalUserId)
	if err != nil {
		return nil, err
	}
	questCrafts, err := uc.GetCraftQuests(internalUserId)
	if err != nil {
		return nil, err
	}
	questCharacters, err := uc.GetCharacterQuests(internalUserId)
	if err != nil {
		return nil, err
	}
	questPlayerOffers, err := uc.GetPlayerOfferQuests(internalUserId)
	if err != nil {
		return nil, err
	}
	info := &model_quest.AllQuestInfo{
		Quests:            questPart1s,
		QuestPart2s:       questPart2s,
		EventQuests:       questEvents,
		CharacterQuests:   questCharacters,
		CraftQuests:       questCrafts,
		PlayerOfferQuests: questPlayerOffers,
	}
	return info, nil
}

func (uc *questUsecase) GetQuest(questId uint) (*model_quest.Quest, error) {
	foundQuest, err := uc.qr.FindQuest(&model_quest.Quest{Id: questId}, nil, []string{"QuestFirstClearReward"})
	if err != nil {
		return nil, err
	}
	return foundQuest, nil
}

func (uc *questUsecase) GetPart1Quests(internalUserId uint) ([]*model_quest.Quest, error) {
	// NOTE: Since GORM ignores zero value, we need to use map instead of struct
	criteria := map[string]interface{}{
		"category": value.QuestCategoryTypeMainPart1,
	}
	foundQuests, err := uc.qr.FindQuests(nil, criteria, []string{"QuestFirstClearReward"})
	if err != nil {
		return nil, err
	}
	return foundQuests, nil
}

func (uc *questUsecase) GetPart2Quests(internalUserId uint) ([]*model_quest.Quest, error) {
	query := &model_quest.Quest{
		Category: value.QuestCategoryTypeMainPart2,
	}
	foundQuests, err := uc.qr.FindQuests(query, nil, []string{"QuestFirstClearReward"})
	if err != nil {
		return nil, err
	}
	return foundQuests, nil
}

func (uc *questUsecase) GetEventQuests(internalUserId uint) ([]*model_quest.EventQuest, error) {
	// TODO: Implement
	foundQuests := []*model_quest.EventQuest{}
	return foundQuests, nil
}

func (uc *questUsecase) GetCharacterQuests(internalUserId uint) ([]*model_quest.CharacterQuest, error) {
	// TODO: Implement
	foundQuests := []*model_quest.CharacterQuest{}
	return foundQuests, nil
}

func (uc *questUsecase) GetCraftQuests(internalUserId uint) ([]*interface{}, error) {
	// TODO: Implement
	foundQuests := []*interface{}{}
	return foundQuests, nil
}

func (uc *questUsecase) GetPlayerOfferQuests(internalUserId uint) ([]*interface{}, error) {
	// TODO: Implement
	foundQuests := []*interface{}{}
	return foundQuests, nil
}
