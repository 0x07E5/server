package middleware

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"
)

func NewLoggerMiddleware(loggerRepo repository.LoggerRepository) func(next http.HandlerFunc) http.HandlerFunc {
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			start := time.Now()
			loggerRepo.Info(
				fmt.Sprintf(
					"%s %s %s",
					start,
					r.Method,
					r.RequestURI,
				),
			)
			next(w, r)
		}
	}
}
