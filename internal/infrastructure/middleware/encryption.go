package middleware

import (
	"bytes"
	"encoding/json"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/encrypt"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

type responseBuffer struct {
	http.ResponseWriter
	buf *bytes.Buffer
}

func (w *responseBuffer) Write(data []byte) (int, error) {
	if w.buf == nil {
		w.buf = new(bytes.Buffer)
	}
	n, err := w.buf.Write(data)
	if err != nil {
		return n, err
	}
	return 0, nil
}

func (w *responseBuffer) WriteHeader(code int) {
	// DO NOT DELETE THIS EMPTY FUNCTION, OTHERWISE THE X-STAR-CC HEADER WILL NOT BE SET
}

func (w *responseBuffer) Buffer() *bytes.Buffer {
	return w.buf
}

func NewAppEncryptionMiddleware(ignoreRoutes *[]string, key *encrypt.Key, padding *encrypt.Pad) func(next http.HandlerFunc) http.HandlerFunc {
	cipher := encrypt.NewSparkleCipher(*key, *padding)
	return func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Server", "Sparkle")
			bufw := &responseBuffer{ResponseWriter: w}
			next(bufw, r)
			rawResp := bufw.Buffer().Bytes()
			var timeReader response.BaseResponse
			if err := json.Unmarshal(rawResp, &timeReader); err != nil {
				w.Write([]byte("Sparkle Server Error: Failed to parse response as JSON"))
				return
			}
			var resp []byte
			if !calc.StrContains(*ignoreRoutes, r.URL.Path) {
				resp = cipher.EncryptAsResponseByBytes(r.URL.Path, rawResp)
				w.Header().Set("kNNHMre7jsDEHFaURYBP", "1")
			} else {
				resp = rawResp
				w.Header().Set("kNNHMre7jsDEHFaURYBP", "0")
			}
			w.Header().Set("X-Star-CC", cipher.GetStarCCByBytes(rawResp, timeReader.ServerTime))
			w.Write(resp)
		}
	}
}
