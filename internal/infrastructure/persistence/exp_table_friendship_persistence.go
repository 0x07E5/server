package persistence

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type expTableFriendshipRepositoryImpl struct {
	Conn *gorm.DB
}

func NewExpTableFriendshipRepositoryImpl(conn *gorm.DB) repository.ExpTableFriendshipRepository {
	return &expTableFriendshipRepositoryImpl{Conn: conn}
}

func (rp *expTableFriendshipRepositoryImpl) FindExpTableFriendships(query *model_exp_table.ExpTableFriendship, criteria map[string]interface{}) ([]*model_exp_table.ExpTableFriendship, error) {
	var datas []*model_exp_table.ExpTableFriendship
	var result *gorm.DB
	chain := rp.Conn
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *expTableFriendshipRepositoryImpl) FindExpTableFriendship(query *model_exp_table.ExpTableFriendship, criteria map[string]interface{}) (*model_exp_table.ExpTableFriendship, error) {
	var data *model_exp_table.ExpTableFriendship
	var result *gorm.DB
	chain := rp.Conn
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		for key, value := range criteria {
			chain = chain.Where(key, value)
		}
		result = chain.First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}
