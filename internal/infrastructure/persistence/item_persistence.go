package persistence

import (
	model_item "gitlab.com/kirafan/sparkle/server/internal/domain/model/item"
	"gitlab.com/kirafan/sparkle/server/internal/domain/repository"

	"gorm.io/gorm"
)

type itemRepositoryImpl struct {
	Conn *gorm.DB
}

func NewItemRepositoryImpl(conn *gorm.DB) repository.ItemRepository {
	return &itemRepositoryImpl{Conn: conn}
}

func (rp *itemRepositoryImpl) FindItems(query *model_item.Item, criteria map[string]interface{}, associations *[]string) ([]*model_item.Item, error) {
	var datas []*model_item.Item
	var result *gorm.DB
	chain := rp.Conn
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(query).Find(&datas)
	} else {
		result = chain.Where(criteria).Find(&datas)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return datas, nil
}

func (rp *itemRepositoryImpl) FindItem(query *model_item.Item, criteria map[string]interface{}, associations *[]string) (*model_item.Item, error) {
	var data *model_item.Item
	var result *gorm.DB
	chain := rp.Conn
	if associations != nil {
		for _, association := range *associations {
			chain = chain.Preload(association)
		}
	}
	if query != nil {
		result = chain.Where(&query).First(&data)
	} else {
		result = chain.Where(criteria).First(&data)
	}
	if result.Error != nil {
		return nil, result.Error
	}
	return data, nil
}
