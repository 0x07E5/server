package seed

import (
	"encoding/json"

	model_mission "gitlab.com/kirafan/sparkle/server/internal/domain/model/mission"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedMissions(db *gorm.DB) {
	missionsFile, err := Read("missions")
	if err != nil {
		return
	}
	var missions []model_mission.Mission
	err = json.Unmarshal(missionsFile, &missions)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(missions, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}
