package seed

import (
	"encoding/json"

	model_offer "gitlab.com/kirafan/sparkle/server/internal/domain/model/offer"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedOffers(db *gorm.DB) {
	offersFile, err := Read("offers")
	if err != nil {
		return
	}
	var offers []model_offer.Offer
	err = json.Unmarshal(offersFile, &offers)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(offers, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}
