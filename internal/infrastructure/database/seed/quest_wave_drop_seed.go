package seed

import (
	"encoding/json"

	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func SeedQuestWaveDrops(db *gorm.DB) {
	questWaveDropFile, err := Read("wave_drop_list")
	if err != nil {
		return
	}
	var questWaveDrops []model_quest.QuestWaveDrop
	err = json.Unmarshal(questWaveDropFile, &questWaveDrops)
	if err != nil {
		panic(err)
	}
	result := db.Clauses(clause.OnConflict{
		DoNothing: true,
	}).CreateInBatches(questWaveDrops, 50)
	if result.Error != nil {
		panic(result.Error)
	}
}
