package database

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func NewMySQLDatabase(dbConfig ConfigList, gormConfig gorm.Config) *gorm.DB {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local", dbConfig.DatabaseUser, dbConfig.DatabasePass, dbConfig.DatabaseHost, dbConfig.DatabasePort, dbConfig.DatabaseName)
	db, err := gorm.Open(mysql.Open(dsn), &gormConfig)
	if err != nil {
		panic(err)
	}
	return db
}

func NewSQLiteDatabase(dbConfig ConfigList, gormConfig gorm.Config) *gorm.DB {
	db, err := gorm.Open(sqlite.Open(dbConfig.DatabaseName), &gormConfig)
	if err != nil {
		panic(err)
	}
	return db
}

func NewSQLiteInMemoryDatabase(dbConfig ConfigList, gormConfig gorm.Config) *gorm.DB {
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gormConfig)
	if err != nil {
		panic(err)
	}
	return db
}
