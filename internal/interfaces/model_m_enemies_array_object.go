/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type MEnemiesArrayObject struct {
	MDropID int64 `json:"m_DropID"`

	MEnemyID int64 `json:"m_EnemyID"`

	MEnemyLv int64 `json:"m_EnemyLv"`

	MScale float32 `json:"m_Scale"`

	Point int64 `json:"point"`
}

// AssertMEnemiesArrayObjectRequired checks if the required fields are not zero-ed
func AssertMEnemiesArrayObjectRequired(obj MEnemiesArrayObject) error {
	elements := map[string]interface{}{
		"m_DropID":  obj.MDropID,
		"m_EnemyID": obj.MEnemyID,
		"m_EnemyLv": obj.MEnemyLv,
		"m_Scale":   obj.MScale,
		"point":     obj.Point,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseMEnemiesArrayObjectRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of MEnemiesArrayObject (e.g. [][]MEnemiesArrayObject), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseMEnemiesArrayObjectRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aMEnemiesArrayObject, ok := obj.(MEnemiesArrayObject)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertMEnemiesArrayObjectRequired(aMEnemiesArrayObject)
	})
}
