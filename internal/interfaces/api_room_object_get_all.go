/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"net/http"
	"strings"
)

// RoomObjectGetAllApiController binds http requests to an api service and writes the service results to the http response
type RoomObjectGetAllApiController struct {
	service      RoomObjectGetAllApiServicer
	errorHandler ErrorHandler
}

// RoomObjectGetAllApiOption for how the controller is set up.
type RoomObjectGetAllApiOption func(*RoomObjectGetAllApiController)

// WithRoomObjectGetAllApiErrorHandler inject ErrorHandler into controller
func WithRoomObjectGetAllApiErrorHandler(h ErrorHandler) RoomObjectGetAllApiOption {
	return func(c *RoomObjectGetAllApiController) {
		c.errorHandler = h
	}
}

// NewRoomObjectGetAllApiController creates a default api controller
func NewRoomObjectGetAllApiController(s RoomObjectGetAllApiServicer, opts ...RoomObjectGetAllApiOption) Router {
	controller := &RoomObjectGetAllApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}

	for _, opt := range opts {
		opt(controller)
	}

	return controller
}

// Routes returns all the api routes for the RoomObjectGetAllApiController
func (c *RoomObjectGetAllApiController) Routes() Routes {
	return Routes{
		{
			"GetAllRoomObject",
			strings.ToUpper("Get"),
			"/api/room_object/get_all",
			c.GetAllRoomObject,
		},
	}
}

// GetAllRoomObject - Get All Room Object (WIP)
func (c *RoomObjectGetAllApiController) GetAllRoomObject(w http.ResponseWriter, r *http.Request) {
	result, err := c.service.GetAllRoomObject(r.Context())
	// If an error occurred, encode the error with the status code
	if err != nil {
		c.errorHandler(w, r, err, &result)
		return
	}
	// If no error, encode the body and the result code
	EncodeJSONResponse(result.Body, &result.Code, w)

}
