/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"context"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx_func"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// PlayerQuestApiService is a service that implements the logic for the PlayerQuestApiServicer
// This service should implement the business logic for every endpoint for the PlayerQuestApi API.
// Include any external packages or services that will be required by this service.
type PlayerQuestApiService struct {
	uu usecase.UserUsecase
	qu usecase.QuestUsecase
}

// NewPlayerQuestApiService creates a default api service
func NewPlayerQuestApiService(uu usecase.UserUsecase, qu usecase.QuestUsecase) PlayerQuestApiServicer {
	return &PlayerQuestApiService{uu, qu}
}

// GetAllPlayerQuest - Get All Player Quest (WIP)
func (s *PlayerQuestApiService) GetAllPlayerQuest(ctx context.Context) (ImplResponse, error) {
	internalId, valid := ctx.Value(ctx_func.CtxUserId).(uint)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	questInfo, err := s.qu.GetAll(internalId)
	if err != nil {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_DB_ERROR)), nil
	}

	// Database to Response model conversion
	success := response.NewSuccessResponse()
	resp := toGetAllPlayerQuestResponse(questInfo, &success)

	return Response(http.StatusOK, resp), nil
}
