package interfaces

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func toGetAllPlayerPresentResponse(presents *[]model_user.UserPresent, success *response.BaseResponse) *GetAllPlayerPresentResponse {
	var outPresents []PresentsArrayObject
	calc.Copy(&outPresents, &presents)
	for i := range outPresents {
		// Fill sub model fields
		calc.Copy(&outPresents[i], (*presents)[i].Present)
		// File date time fields
		outPresents[i].CreatedAt = response.ToSparkleTime((*presents)[i].CreatedAt)
		outPresents[i].DeadlineAt = response.ToSparkleTime((*presents)[i].DeadlineAt)
		if (*presents)[i].ReceivedAt != nil {
			outPresents[i].ReceivedAt = response.ToSparkleTime(*(*presents)[i].ReceivedAt)
		} else {
			outPresents[i].ReceivedAt = "0001-01-01T00:00:00"
		}
	}
	return &GetAllPlayerPresentResponse{
		Presents:            outPresents,
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}
}
