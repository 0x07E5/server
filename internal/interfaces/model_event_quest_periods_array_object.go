/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

type EventQuestPeriodsArrayObject struct {
	EndAt string `json:"endAt"`

	EventQuestPeriodGroups []EventQuestPeriodGroupsArrayObject `json:"eventQuestPeriodGroups"`

	EventType int64 `json:"eventType"`

	LossTimeEndAt *string `json:"lossTimeEndAt"`

	StartAt string `json:"startAt"`
}

// AssertEventQuestPeriodsArrayObjectRequired checks if the required fields are not zero-ed
func AssertEventQuestPeriodsArrayObjectRequired(obj EventQuestPeriodsArrayObject) error {
	elements := map[string]interface{}{
		"endAt":                  obj.EndAt,
		"eventQuestPeriodGroups": obj.EventQuestPeriodGroups,
		"eventType":              obj.EventType,
		"lossTimeEndAt":          obj.LossTimeEndAt,
		"startAt":                obj.StartAt,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	for _, el := range obj.EventQuestPeriodGroups {
		if err := AssertEventQuestPeriodGroupsArrayObjectRequired(el); err != nil {
			return err
		}
	}
	return nil
}

// AssertRecurseEventQuestPeriodsArrayObjectRequired recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of EventQuestPeriodsArrayObject (e.g. [][]EventQuestPeriodsArrayObject), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseEventQuestPeriodsArrayObjectRequired(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aEventQuestPeriodsArrayObject, ok := obj.(EventQuestPeriodsArrayObject)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertEventQuestPeriodsArrayObjectRequired(aEventQuestPeriodsArrayObject)
	})
}
