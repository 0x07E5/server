package interfaces

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func toGetAllPlayerFavoriteMemberResponse(userFavoriteMembers *[]model_user.FavoriteMember, success *response.BaseResponse) *GetAllPlayerFavoriteMemberResponse {
	var favoriteMembers []ManagedFavoriteMembersArrayObject
	calc.Copy(&favoriteMembers, &userFavoriteMembers)

	return &GetAllPlayerFavoriteMemberResponse{
		ManagedFavoriteMembers: favoriteMembers,
		NewAchievementCount:    success.NewAchievementCount,
		ResultCode:             success.ResultCode,
		ResultMessage:          success.ResultMessage,
		ServerTime:             success.ServerTime,
		ServerVersion:          success.ServerVersion,
	}
}
