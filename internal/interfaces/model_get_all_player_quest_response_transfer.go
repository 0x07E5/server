package interfaces

import (
	model_quest "gitlab.com/kirafan/sparkle/server/internal/domain/model/quest"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// FIXME: There is no way to separate this DTO(database to response model conversion)
// The DTO depends interfaces package's models, but this router also located at interfaces package
// So, I just put this conversion code here.
// If you have any idea, please let me know.

func toGetAllPlayerQuestResponse(dbQuests *model_quest.AllQuestInfo, success *response.BaseResponse) *GetAllPlayerQuestResponse {
	// Format resp->quests field
	var outQuests []QuestsArrayObject
	var copiedQuests []QuestsArrayObjectQuest
	calc.Copy(&copiedQuests, &dbQuests.Quests)
	for i := range copiedQuests {
		// Fill duplicated value
		copiedQuests[i].WaveIds = []int64{
			copiedQuests[i].WaveId1,
			copiedQuests[i].WaveId2,
			copiedQuests[i].WaveId3,
			copiedQuests[i].WaveId4,
			copiedQuests[i].WaveId5,
		}
		calc.Copy(&copiedQuests[i], &copiedQuests[i].QuestFirstClearReward)
		copiedQuests[i].Gem1 = copiedQuests[i].QuestFirstClearReward.Gem
		copiedQuests[i].Gold1 = copiedQuests[i].QuestFirstClearReward.Gold
		// Fill Mystery values
		copiedQuests[i].MainFlg = 0
		copiedQuests[i].Section = -1
		// Fill empty array
		if len(copiedQuests[i].QuestNpcs) == 0 {
			copiedQuests[i].QuestNpcs = make([]QuestNpcsArrayObject, 0)
		}
		// Finalize QuestsArrayObject
		outQuests = append(outQuests, QuestsArrayObject{
			ClearRank: 3,
			Quest:     copiedQuests[i],
		})
	}
	// Format resp->questPart2s field
	var outQuestPart2s []Questpart2sArrayObject
	var copiedQuestPart2s []Questpart2sArrayObjectQuest
	calc.Copy(&copiedQuestPart2s, &dbQuests.QuestPart2s)
	for i := range copiedQuestPart2s {
		// Fill duplicated value
		copiedQuestPart2s[i].WaveIds = []int64{
			copiedQuestPart2s[i].WaveId1,
			copiedQuestPart2s[i].WaveId2,
			copiedQuestPart2s[i].WaveId3,
			copiedQuestPart2s[i].WaveId4,
			copiedQuestPart2s[i].WaveId5,
		}
		calc.Copy(&copiedQuestPart2s[i], &copiedQuestPart2s[i].QuestFirstClearReward)
		copiedQuestPart2s[i].Gem1 = copiedQuestPart2s[i].QuestFirstClearReward.Gem
		copiedQuestPart2s[i].Gold1 = copiedQuestPart2s[i].QuestFirstClearReward.Gold
		// Fill Mystery values
		copiedQuestPart2s[i].MainFlg = 0
		copiedQuestPart2s[i].Section = -1
		// Fill empty array
		if len(copiedQuestPart2s[i].QuestNpcs) == 0 {
			copiedQuestPart2s[i].QuestNpcs = make([]QuestNpcsArrayObject, 0)
		}
		// Finalize Questpart2sArrayObject
		outQuestPart2s = append(outQuestPart2s, Questpart2sArrayObject{
			ClearRank: 3,
			Quest:     copiedQuestPart2s[i],
		})
	}

	return &GetAllPlayerQuestResponse{
		Quests:                 outQuests,
		QuestPart2s:            outQuestPart2s,
		QuestStaminaReductions: []QuestStaminaReductionsArrayObject{},
		EventQuestPeriods:      []EventQuestPeriodsArrayObject{},
		CharacterQuests:        []CharacterQuestsArrayObject{},
		EventQuests:            []EventQuestsArrayObject{},
		PlayerOfferQuests:      []PlayerOfferQuestsArrayObject{},
		// TODO: Get correct values from user usecase
		LastPlayedChapterQuestIds: []int64{-1, -1},
		PlayedOpenChapterIds:      []int64{-1, -1},
		ReadGroups:                make([]interface{}, 0),
		// Template responses
		ServerVersion:       success.ServerVersion,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		NewAchievementCount: success.NewAchievementCount,
	}
}
