/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

// MissionlogsArrayObject2 - Request type of /player/mission/set
type MissionlogsArrayObject2 struct {
	Category int64 `json:"category"`

	LimitTime string `json:"limitTime"`

	ManagedMissionId int64 `json:"managedMissionId"`

	MissionFuncType int64 `json:"missionFuncType"`

	MissionID int64 `json:"missionID"`

	MissionSegType int64 `json:"missionSegType"`

	Rate int64 `json:"rate"`

	RateMax int64 `json:"rateMax"`

	Reward *string `json:"reward"`

	State int64 `json:"state"`

	SubCode int64 `json:"subCode"`

	// default is null
	TargetMessage *string `json:"targetMessage"`

	TransitParam int64 `json:"transitParam"`

	// Mystery, static 0
	TransitScene int64 `json:"transitScene"`

	// Mystery, static 0
	UiPriority int64 `json:"uiPriority"`
}

// AssertMissionlogsArrayObject2Required checks if the required fields are not zero-ed
func AssertMissionlogsArrayObject2Required(obj MissionlogsArrayObject2) error {
	elements := map[string]interface{}{
		"limitTime":        obj.LimitTime,
		"managedMissionId": obj.ManagedMissionId,
		"missionID":        obj.MissionID,
		"rate":             obj.Rate,
		"rateMax":          obj.RateMax,
	}
	for name, el := range elements {
		if isZero := IsZeroValue(el); isZero {
			return &RequiredError{Field: name}
		}
	}

	return nil
}

// AssertRecurseMissionlogsArrayObject2Required recursively checks if required fields are not zero-ed in a nested slice.
// Accepts only nested slice of MissionlogsArrayObject2 (e.g. [][]MissionlogsArrayObject2), otherwise ErrTypeAssertionError is thrown.
func AssertRecurseMissionlogsArrayObject2Required(objSlice interface{}) error {
	return AssertRecurseInterfaceRequired(objSlice, func(obj interface{}) error {
		aMissionlogsArrayObject2, ok := obj.(MissionlogsArrayObject2)
		if !ok {
			return ErrTypeAssertionError
		}
		return AssertMissionlogsArrayObject2Required(aMissionlogsArrayObject2)
	})
}
