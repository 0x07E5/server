package interfaces

func NewInjectableAppVersionApiController(s AppVersionApiServicer) *AppVersionApiController {
	controller := &AppVersionApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectableEventBannerGetAllApiController(s EventBannerGetAllApiServicer) *EventBannerGetAllApiController {
	controller := &EventBannerGetAllApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectableInformationGetAllApiController(s InformationGetAllApiServicer) *InformationGetAllApiController {
	controller := &InformationGetAllApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerAbilityApiController(s PlayerAbilityApiServicer) *PlayerAbilityApiController {
	controller := &PlayerAbilityApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerAchievementApiController(s PlayerAchievementApiServicer) *PlayerAchievementApiController {
	controller := &PlayerAchievementApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerAdvApiController(s PlayerAdvApiServicer) *PlayerAdvApiController {
	controller := &PlayerAdvApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerAgeApiController(s PlayerAgeApiServicer) *PlayerAgeApiController {
	controller := &PlayerAgeApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerBadgeApiController(s PlayerBadgeApiServicer) *PlayerBadgeApiController {
	controller := &PlayerBadgeApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerBattlePartyApiController(s PlayerBattlePartyApiServicer) *PlayerBattlePartyApiController {
	controller := &PlayerBattlePartyApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerCharacterApiController(s PlayerCharacterApiServicer) *PlayerCharacterApiController {
	controller := &PlayerCharacterApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerChestApiController(s PlayerChestApiServicer) *PlayerChestApiController {
	controller := &PlayerChestApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerContentRoomApiController(s PlayerContentRoomApiServicer) *PlayerContentRoomApiController {
	controller := &PlayerContentRoomApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerExchangeShopApiController(s PlayerExchangeShopApiServicer) *PlayerExchangeShopApiController {
	controller := &PlayerExchangeShopApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerFavoriteMemberApiController(s PlayerFavoriteMemberApiServicer) *PlayerFavoriteMemberApiController {
	controller := &PlayerFavoriteMemberApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerFieldPartyApiController(s PlayerFieldPartyApiServicer) *PlayerFieldPartyApiController {
	controller := &PlayerFieldPartyApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerFriendApiController(s PlayerFriendApiServicer) *PlayerFriendApiController {
	controller := &PlayerFriendApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerGachaApiController(s PlayerGachaApiServicer) *PlayerGachaApiController {
	controller := &PlayerGachaApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerGeneralFlagSaveApiController(s PlayerGeneralFlagSaveApiServicer) *PlayerGeneralFlagSaveApiController {
	controller := &PlayerGeneralFlagSaveApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerGetAllApiController(s PlayerGetAllApiServicer) *PlayerGetAllApiController {
	controller := &PlayerGetAllApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerItemApiController(s PlayerItemApiServicer) *PlayerItemApiController {
	controller := &PlayerItemApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerLoginApiController(s PlayerLoginApiServicer) *PlayerLoginApiController {
	controller := &PlayerLoginApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerLoginBonusApiController(s PlayerLoginBonusApiServicer) *PlayerLoginBonusApiController {
	controller := &PlayerLoginBonusApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerMasterOrbApiController(s PlayerMasterOrbApiServicer) *PlayerMasterOrbApiController {
	controller := &PlayerMasterOrbApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerMissionApiController(s PlayerMissionApiServicer) *PlayerMissionApiController {
	controller := &PlayerMissionApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerMoveApiController(s PlayerMoveApiServicer) *PlayerMoveApiController {
	controller := &PlayerMoveApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerOfferApiController(s PlayerOfferApiServicer) *PlayerOfferApiController {
	controller := &PlayerOfferApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerPresentApiController(s PlayerPresentApiServicer) *PlayerPresentApiController {
	controller := &PlayerPresentApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerPushNotificationApiController(s PlayerPushNotificationApiServicer) *PlayerPushNotificationApiController {
	controller := &PlayerPushNotificationApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerPushTokenApiController(s PlayerPushTokenApiServicer) *PlayerPushTokenApiController {
	controller := &PlayerPushTokenApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerQuestApiController(s PlayerQuestApiServicer) *PlayerQuestApiController {
	controller := &PlayerQuestApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerQuestLogApiController(s PlayerQuestLogApiServicer) *PlayerQuestLogApiController {
	controller := &PlayerQuestLogApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerResetApiController(s PlayerResetApiServicer) *PlayerResetApiController {
	controller := &PlayerResetApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerResumeApiController(s PlayerResumeApiServicer) *PlayerResumeApiController {
	controller := &PlayerResumeApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerRoomApiController(s PlayerRoomApiServicer) *PlayerRoomApiController {
	controller := &PlayerRoomApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerRoomObjectApiController(s PlayerRoomObjectApiServicer) *PlayerRoomObjectApiController {
	controller := &PlayerRoomObjectApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerScheduleApiController(s PlayerScheduleApiServicer) *PlayerScheduleApiController {
	controller := &PlayerScheduleApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerSetApiController(s PlayerSetApiServicer) *PlayerSetApiController {
	controller := &PlayerSetApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerSignupApiController(s PlayerSignupApiServicer) *PlayerSignupApiController {
	controller := &PlayerSignupApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerStaminaApiController(s PlayerStaminaApiServicer) *PlayerStaminaApiController {
	controller := &PlayerStaminaApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerStoreApiController(s PlayerStoreApiServicer) *PlayerStoreApiController {
	controller := &PlayerStoreApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerTownApiController(s PlayerTownApiServicer) *PlayerTownApiController {
	controller := &PlayerTownApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerTownFacilityApiController(s PlayerTownFacilityApiServicer) *PlayerTownFacilityApiController {
	controller := &PlayerTownFacilityApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerTrainingApiController(s PlayerTrainingApiServicer) *PlayerTrainingApiController {
	controller := &PlayerTrainingApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerTutorialApiController(s PlayerTutorialApiServicer) *PlayerTutorialApiController {
	controller := &PlayerTutorialApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectablePlayerWeaponApiController(s PlayerWeaponApiServicer) *PlayerWeaponApiController {
	controller := &PlayerWeaponApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectableQuestChapterGetAllApiController(s QuestChapterGetAllApiServicer) *QuestChapterGetAllApiController {
	controller := &QuestChapterGetAllApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}

func NewInjectableRoomObjectGetAllApiController(s RoomObjectGetAllApiServicer) *RoomObjectGetAllApiController {
	controller := &RoomObjectGetAllApiController{
		service:      s,
		errorHandler: DefaultErrorHandler,
	}
	return controller
}
