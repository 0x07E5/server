/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"context"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// PlayerMoveApiService is a service that implements the logic for the PlayerMoveApiServicer
// This service should implement the business logic for every endpoint for the PlayerMoveApi API.
// Include any external packages or services that will be required by this service.
type PlayerMoveApiService struct {
}

// NewPlayerMoveApiService creates a default api service
func NewPlayerMoveApiService() PlayerMoveApiServicer {
	return &PlayerMoveApiService{}
}

// GetPlayerMove - Get Player Move (WIP)
func (s *PlayerMoveApiService) GetPlayerMove(ctx context.Context, getPlayerMoveRequest GetPlayerMoveRequest) (ImplResponse, error) {
	// TODO - update GetPlayerMove with the required logic for this service method.
	// Add api_player_move_service.go to the .openapi-generator-ignore to avoid overwriting this service implementation when updating open api generation.

	//TODO: Uncomment the next line to return response Response(http.StatusOK, GetPlayerMoveResponse{}) or use other options such as http.Ok ...
	//return Response(http.StatusOK, GetPlayerMoveResponse{}), nil

	// STUB
	return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_UNAVAILABLE)), nil
}

// SetPlayerMove - Set Player Move (WIP)
func (s *PlayerMoveApiService) SetPlayerMove(ctx context.Context, setPlayerMoveRequest SetPlayerMoveRequest) (ImplResponse, error) {
	// TODO - update SetPlayerMove with the required logic for this service method.
	// Add api_player_move_service.go to the .openapi-generator-ignore to avoid overwriting this service implementation when updating open api generation.

	//TODO: Uncomment the next line to return response Response(http.StatusOK, SetPlayerMoveResponse{}) or use other options such as http.Ok ...
	//return Response(http.StatusOK, SetPlayerMoveResponse{}), nil

	// STUB
	failed := response.NewErrorResponse(response.RESULT_PLAYER_NOT_FOUND)
	return Response(http.StatusOK, SetPlayerMoveResponse{
		AccessToken:         "",
		PlayerId:            0,
		NewAchievementCount: failed.NewAchievementCount,
		ResultCode:          failed.ResultCode,
		ResultMessage:       failed.ResultMessage,
		ServerTime:          failed.ServerTime,
		ServerVersion:       failed.ServerVersion,
	}), nil
}
