package interfaces

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func toGetAllPlayerFieldPartyMemberResponse(userFieldPartyMembers *[]model_user.ManagedFieldPartyMember, success *response.BaseResponse) *GetAllPlayerFieldPartyMemberResponse {
	var managedFieldPartyMembers []FieldPartyMembersArrayObject
	calc.Copy(&managedFieldPartyMembers, &userFieldPartyMembers)

	// Maybe these values are unused at client...
	for i := 0; i < len(*userFieldPartyMembers); i++ {
		managedFieldPartyMembers[i].ScheduleTable = nil
		managedFieldPartyMembers[i].Character = nil
		managedFieldPartyMembers[i].PartyDropPresents = nil
	}

	return &GetAllPlayerFieldPartyMemberResponse{
		FieldPartyMembers:   managedFieldPartyMembers,
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}
}
