package interfaces

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

func toDrawPlayerGachaResponse(user *model_user.User, gachaResults []value.GachaResult, success *response.BaseResponse) *DrawPlayerGachaResponse {
	var outGachaResults []GachaResultsArrayObject
	calc.Copy(&outGachaResults, &gachaResults)

	var outItemSummary []ItemSummaryArrayObject
	calc.Copy(&outItemSummary, &user.ItemSummary)

	var outManagedCharacters []ManagedCharactersArrayObject
	calc.Copy(&outManagedCharacters, &user.ManagedCharacters)

	var outManagedNamedTypes []ManagedNamedTypesArrayObject
	calc.Copy(&outManagedNamedTypes, &user.ManagedNamedTypes)

	var outOfferTitleTypes []OfferTitleTypesArrayObject
	calc.Copy(&outOfferTitleTypes, &user.OfferTitleTypes)

	var outPlayer BasePlayer
	calc.Copy(&outPlayer, &user)
	outPlayer.CreatedAt = response.ToSparkleTime(user.CreatedAt)
	outPlayer.LastLoginAt = response.ToSparkleTime(user.LastLoginAt)
	outPlayer.StaminaUpdatedAt = response.ToSparkleTime(user.StaminaUpdatedAt)

	return &DrawPlayerGachaResponse{
		// REAL
		GachaResults:      outGachaResults,
		ItemSummary:       outItemSummary,
		ManagedCharacters: outManagedCharacters,
		ManagedNamedTypes: outManagedNamedTypes,
		OfferTitleTypes:   outOfferTitleTypes,
		Player:            outPlayer,
		// STUB
		BeforeDrawPoint:     1204,
		AfterDrawPoint:      1204,
		ArousalResults:      make([]ArousalResultsArrayObject, 0),
		GachaBonuses:        make([]GachaBonusesArrayObject, 0),
		Offers:              make([]interface{}, 0),
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}
}
