/*
 * SparkleAPI
 *
 * \"Our tomorrow is always a prologue\"  This doc is API Reference and template to generate krr-prd.star-api mirror.
 *
 * API version: 1.0
 * Contact: contact@sparklefantasia.com
 * Generated by: OpenAPI Generator (https://openapi-generator.tech)
 */

package interfaces

import (
	"context"
	"errors"
	"net/http"

	"gitlab.com/kirafan/sparkle/server/internal/application/service"
	"gitlab.com/kirafan/sparkle/server/internal/application/usecase"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	"gitlab.com/kirafan/sparkle/server/pkg/ctx_func"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// PlayerQuestLogApiService is a service that implements the logic for the PlayerQuestLogApiServicer
// This service should implement the business logic for every endpoint for the PlayerQuestLogApi API.
// Include any external packages or services that will be required by this service.
type PlayerQuestLogApiService struct {
	uu usecase.UserUsecase
	qs service.UserQuestService
}

// NewPlayerQuestLogApiService creates a default api service
func NewPlayerQuestLogApiService(uu usecase.UserUsecase, qs service.UserQuestService) PlayerQuestLogApiServicer {
	return &PlayerQuestLogApiService{uu, qs}
}

// CompletePlayerQuestLog - Complete Player Quest Log (WIP)
func (s *PlayerQuestLogApiService) CompletePlayerQuestLog(ctx context.Context, completePlayerQuestLogRequest CompletePlayerQuestLogRequest) (ImplResponse, error) {
	// Validate playerId
	internalId, valid := ctx.Value(ctx_func.CtxUserId).(uint)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}
	// Validate stepCode
	stepCode, err := value.NewStepCode(int8(completePlayerQuestLogRequest.StepCode))
	if err != nil {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
	}

	// completePlayerQuestLogRequest.State
	// 1: Quest failed
	// 2: Quest cleared
	user, isFirstClear, err := s.qs.CompleteQuest(
		internalId,
		uint(completePlayerQuestLogRequest.OrderReceiveId),
		uint8(completePlayerQuestLogRequest.State),
		uint8(completePlayerQuestLogRequest.ClearRank),
		completePlayerQuestLogRequest.SkillExps,
		completePlayerQuestLogRequest.WeaponSkillExps,
		uint8(completePlayerQuestLogRequest.FriendUseNum),
		uint8(completePlayerQuestLogRequest.MasterSkillUseNum),
		uint8(completePlayerQuestLogRequest.UniqueSkillUseNum),
		stepCode,
	)
	if err != nil {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_DB_ERROR)), nil
	}

	success := response.NewSuccessResponse()
	resp := toCompletePlayerQuestLogResponse(user, &success, isFirstClear)
	return Response(http.StatusOK, resp), nil
}

// OrderPlayerQuestLog - Order Player Quest Log (WIP)
func (s *PlayerQuestLogApiService) OrderPlayerQuestLog(ctx context.Context, orderPlayerQuestLogRequest OrderPlayerQuestLogRequest) (ImplResponse, error) {
	// Validate playerId
	internalId, valid := ctx.Value(ctx_func.CtxUserId).(uint)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}
	user, waves, drops, err := s.qs.StartQuest(
		internalId,
		uint(orderPlayerQuestLogRequest.QuestId),
		uint(orderPlayerQuestLogRequest.ManagedBattlePartyId),
		orderPlayerQuestLogRequest.SupportCharacterId,
		orderPlayerQuestLogRequest.QuestNpcId,
	)
	if err != nil {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_INVALID_PARAMETERS)), nil
	}

	success := response.NewSuccessResponse()
	resp := toOrderPlayerQuestLogResponse(user, waves, drops, &success)
	return Response(http.StatusOK, resp), nil
}

// ResetPlayerQuestLog - Reset Player Quest Log (WIP)
func (s *PlayerQuestLogApiService) ResetPlayerQuestLog(ctx context.Context, body map[string]interface{}) (ImplResponse, error) {
	// TODO - update ResetPlayerQuestLog with the required logic for this service method.
	// Add api_player_quest_log_service.go to the .openapi-generator-ignore to avoid overwriting this service implementation when updating open api generation.

	//TODO: Uncomment the next line to return response Response(http.StatusOK, ResetPlayerQuestLogResponse{}) or use other options such as http.Ok ...
	//return Response(http.StatusOK, ResetPlayerQuestLogResponse{}), nil

	return Response(http.StatusNotImplemented, nil), errors.New("ResetPlayerQuestLog method not implemented")
}

// RetryPlayerQuestLog - Retry Player Quest Log (WIP)
func (s *PlayerQuestLogApiService) RetryPlayerQuestLog(ctx context.Context, retryPlayerQuestLogRequest RetryPlayerQuestLogRequest) (ImplResponse, error) {
	// TODO - update RetryPlayerQuestLog with the required logic for this service method.
	// Add api_player_quest_log_service.go to the .openapi-generator-ignore to avoid overwriting this service implementation when updating open api generation.

	//TODO: Uncomment the next line to return response Response(http.StatusOK, RetryPlayerQuestLogResponse{}) or use other options such as http.Ok ...
	//return Response(http.StatusOK, RetryPlayerQuestLogResponse{}), nil

	return Response(http.StatusNotImplemented, nil), errors.New("RetryPlayerQuestLog method not implemented")
}

// SavePlayerQuestLog - Save Player Quest Log (WIP)
func (s *PlayerQuestLogApiService) SavePlayerQuestLog(ctx context.Context, savePlayerQuestLogRequest SavePlayerQuestLogRequest) (ImplResponse, error) {
	// Validate playerId
	internalId, valid := ctx.Value(ctx_func.CtxUserId).(uint)
	if !valid {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_PLAYER_SESSION_EXPIRED)), nil
	}

	err := s.qs.SaveQuest(
		internalId,
		int(savePlayerQuestLogRequest.OrderReceiveId),
		savePlayerQuestLogRequest.QuestData,
	)
	if err != nil {
		return Response(http.StatusOK, response.NewErrorResponse(response.RESULT_DB_ERROR)), nil
	}

	success := response.NewSuccessResponse()
	resp := toSavePlayerQuestLogResponse(&success)
	return Response(http.StatusOK, resp), nil
}
