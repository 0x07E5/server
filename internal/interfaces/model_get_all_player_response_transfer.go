package interfaces

import (
	model_user "gitlab.com/kirafan/sparkle/server/internal/domain/model/user"
	"gitlab.com/kirafan/sparkle/server/pkg/response"
)

// FIXME: There is no way to separate this DTO(database to response model conversion)
// The DTO depends interfaces package's models, but this router also located at interfaces package
// So, I just put this conversion code here.
// If you have any idea, please let me know.

func toItemSummaryResponse(itemSummary []model_user.ItemSummary) []ItemSummaryArrayObject {
	itemSummaryResp := make([]ItemSummaryArrayObject, len(itemSummary))
	for i := range itemSummary {
		itemSummaryResp[i] = ItemSummaryArrayObject{
			Id:     int64(itemSummary[i].Id),
			Amount: int64(itemSummary[i].Amount),
		}
	}
	return itemSummaryResp
}

func ToManagedRoomObjectsResponse(managedRoomObjects []model_user.ManagedRoomObject) []ManagedRoomObjectsArrayObject {
	managedRoomObjectsResp := make([]ManagedRoomObjectsArrayObject, len(managedRoomObjects))
	for i := range managedRoomObjects {
		managedRoomObjectsResp[i] = ManagedRoomObjectsArrayObject{
			ManagedRoomObjectId: int64(managedRoomObjects[i].ManagedRoomObjectId),
			RoomObjectId:        int64(managedRoomObjects[i].RoomObjectId),
		}
	}
	return managedRoomObjectsResp
}

func ToManagedRoomsResponse(managedRooms []model_user.ManagedRoom) []ManagedRoomsArrayObject {
	managedRoomsResp := make([]ManagedRoomsArrayObject, len(managedRooms))
	for i := range managedRooms {
		arrangeData := make([]ArrangeDataArrayObject, len(managedRooms[i].ArrangeData))
		for i2 := range managedRooms[i].ArrangeData {
			arrangeData[i2] = ArrangeDataArrayObject{
				Dir:                 int64(managedRooms[i].ArrangeData[i2].Dir),
				ManagedRoomObjectId: int64(managedRooms[i].ArrangeData[i2].ManagedRoomObjectId),
				RoomNo:              int64(managedRooms[i].ArrangeData[i2].RoomNo),
				RoomObjectId:        int64(managedRooms[i].ArrangeData[i2].RoomObjectId),
				X:                   int64(managedRooms[i].ArrangeData[i2].X),
				Y:                   int64(managedRooms[i].ArrangeData[i2].Y),
			}
		}
		managedRoomsResp[i] = ManagedRoomsArrayObject{
			ManagedRoomId: int64(managedRooms[i].ManagedRoomId),
			FloorId:       int64(managedRooms[i].FloorId),
			GroupId:       int64(managedRooms[i].GroupId),
			Active:        int64(managedRooms[i].Active),
			ArrangeData:   arrangeData,
		}
	}
	return managedRoomsResp
}

func toGetAllPlayerResponse(p *model_user.User, success *response.BaseResponse) *GetAllPlayerResponse {
	var advIds []int64
	for _, advObj := range p.AdvIds {
		advIds = append(advIds, int64(advObj.AdvId))
	}
	if len(p.AdvIds) == 0 {
		advIds = make([]int64, 0)
	}

	var favoriteMembers []FavoriteMembersArrayObject
	for _, obj := range p.FavoriteMembers {
		favoriteMembers = append(favoriteMembers, FavoriteMembersArrayObject{
			ArousalLevel:       int64(obj.ArousalLevel),
			CharacterId:        int64(obj.CharacterId),
			FavoriteIndex:      int64(obj.FavoriteIndex),
			ManagedCharacterId: int64(obj.ManagedCharacterId),
		})
	}
	if len(p.FavoriteMembers) == 0 {
		favoriteMembers = make([]FavoriteMembersArrayObject, 0)
	}

	var itemSummary []ItemSummaryArrayObject
	for _, obj := range p.ItemSummary {
		itemSummary = append(itemSummary, ItemSummaryArrayObject{
			Id:     int64(obj.Id),
			Amount: int64(obj.Amount),
		})
	}
	if len(p.ItemSummary) == 0 {
		itemSummary = make([]ItemSummaryArrayObject, 0)
	}

	var managedAbilityBoards []PlayerAbilityBoardsArrayObject
	for _, obj := range p.ManagedAbilityBoards {
		var equipItemIds []int64
		for _, equipItemObj := range obj.EquipItemIds {
			equipItemIds = append(equipItemIds, int64(equipItemObj.ItemId))
		}
		managedAbilityBoards = append(managedAbilityBoards, PlayerAbilityBoardsArrayObject{
			ManagedAbilityBoardId: int64(obj.ManagedAbilityBoardId),
			AbilityBoardId:        int64(obj.AbilityBoardId),
			ManagedCharacterId:    int64(obj.ManagedCharacterId),
			EquipItemIds:          equipItemIds,
		})
	}
	if len(p.ManagedAbilityBoards) == 0 {
		managedAbilityBoards = make([]PlayerAbilityBoardsArrayObject, 0)
	}

	var managedBattleParties []ManagedBattlePartiesArrayObject
	for _, obj := range p.ManagedBattleParties {
		managedBattleParties = append(managedBattleParties, ManagedBattlePartiesArrayObject{
			CostLimit:            int64(obj.CostLimit),
			ManagedBattlePartyId: int64(obj.ManagedBattlePartyId),
			ManagedCharacterId1:  int64(obj.ManagedCharacterId1),
			ManagedCharacterId2:  int64(obj.ManagedCharacterId2),
			ManagedCharacterId3:  int64(obj.ManagedCharacterId3),
			ManagedCharacterId4:  int64(obj.ManagedCharacterId4),
			ManagedCharacterId5:  int64(obj.ManagedCharacterId5),
			ManagedCharacterIds: []int64{
				obj.ManagedCharacterId1,
				obj.ManagedCharacterId2,
				obj.ManagedCharacterId3,
				obj.ManagedCharacterId4,
				obj.ManagedCharacterId5,
			},
			ManagedWeaponId1: int64(obj.ManagedWeaponId1),
			ManagedWeaponId2: int64(obj.ManagedWeaponId2),
			ManagedWeaponId3: int64(obj.ManagedWeaponId3),
			ManagedWeaponId4: int64(obj.ManagedWeaponId4),
			ManagedWeaponId5: int64(obj.ManagedWeaponId5),
			ManagedWeaponIds: []int64{
				int64(obj.ManagedWeaponId1),
				int64(obj.ManagedWeaponId2),
				int64(obj.ManagedWeaponId3),
				int64(obj.ManagedWeaponId4),
				int64(obj.ManagedWeaponId5),
			},
			MasterOrbId: int64(obj.MasterOrbId),
			Name:        obj.Name,
		})
	}
	var managedCharacters []ManagedCharactersArrayObject
	for _, obj := range p.ManagedCharacters {
		managedCharacters = append(managedCharacters, ManagedCharactersArrayObject{
			ArousalLevel:       int64(obj.ArousalLevel),
			CharacterId:        int64(obj.CharacterId),
			DuplicatedCount:    int64(obj.DuplicatedCount),
			Exp:                int64(obj.Exp),
			Level:              int64(obj.Level),
			LevelBreak:         int64(obj.LevelBreak),
			LevelLimit:         int64(obj.LevelLimit),
			ManagedCharacterId: int64(obj.ManagedCharacterId),
			PlayerId:           int64(obj.PlayerId),
			Shown:              int64(obj.Shown),
			SkillExp1:          int64(obj.SkillExp1),
			SkillExp2:          int64(obj.SkillExp2),
			SkillExp3:          int64(obj.SkillExp3),
			SkillLevel1:        int64(obj.SkillLevel1),
			SkillLevel2:        int64(obj.SkillLevel2),
			SkillLevel3:        int64(obj.SkillLevel3),
			SkillLevelLimit1:   int64(obj.SkillLevelLimit1),
			SkillLevelLimit2:   int64(obj.SkillLevelLimit2),
			SkillLevelLimit3:   int64(obj.SkillLevelLimit3),
			ViewCharacterId:    int64(obj.ViewCharacterId),
		})
	}
	if len(p.ManagedCharacters) == 0 {
		managedCharacters = make([]ManagedCharactersArrayObject, 0)
	}

	var managedFieldPartyMembers []ManagedFieldPartyMembersArrayObject
	for _, obj := range p.ManagedFieldPartyMembers {
		base := ManagedFieldPartyMembersArrayObject{
			ArousalLevel:         0,
			Character:            nil,
			CharacterId:          int64(obj.CharacterId),
			Flag:                 int64(obj.Flag),
			LiveIdx:              int64(obj.LiveIdx),
			ManagedCharacterId:   int64(obj.ManagedCharacterId),
			ManagedFacilityId:    int64(obj.ManagedFacilityId),
			ManagedPartyMemberId: int64(obj.ManagedPartyMemberId),
			PartyDropPresents:    nil,
			RoomId:               int64(obj.RoomId),
			ScheduleId:           int64(obj.ScheduleId),
			ScheduleTag:          int64(obj.ScheduleTag),
			TouchItemResultNo:    int64(obj.TouchItemResultNo),
		}
		if obj.ScheduleTable != nil {
			base.ScheduleTable = *obj.ScheduleTable
		}
		managedFieldPartyMembers = append(managedFieldPartyMembers, base)
	}
	if len(p.ManagedFieldPartyMembers) == 0 {
		managedFieldPartyMembers = make([]ManagedFieldPartyMembersArrayObject, 0)
	}

	var managedMasterOrbs []ManagedMasterOrbsArrayObject
	for _, obj := range p.ManagedMasterOrbs {
		managedMasterOrbs = append(managedMasterOrbs, ManagedMasterOrbsArrayObject{
			Exp:                int64(obj.Exp),
			Level:              int64(obj.Level),
			ManagedMasterOrbId: int64(obj.ManagedMasterOrbId),
			MasterOrbId:        int64(obj.MasterOrbId),
		})
	}
	var managedNamedTypes []ManagedNamedTypesArrayObject
	for _, obj := range p.ManagedNamedTypes {
		managedNamedTypes = append(managedNamedTypes, ManagedNamedTypesArrayObject{
			Exp:                  int64(obj.Exp),
			FriendshipExpTableId: int64(obj.FriendshipExpTableId),
			Level:                int64(obj.Level),
			ManagedNamedTypeId:   int64(obj.ManagedNamedTypeId),
			NamedType:            int64(obj.NamedType),
			TitleType:            int64(obj.TitleType),
		})
	}
	if len(p.ManagedNamedTypes) == 0 {
		managedNamedTypes = make([]ManagedNamedTypesArrayObject, 0)
	}

	managedRoomObjects := ToManagedRoomObjectsResponse(p.ManagedRoomObjects)
	managedRooms := ToManagedRoomsResponse(p.ManagedRooms)

	var managedTownFacilities []ManagedTownFacilitiesArrayObject
	for _, obj := range p.ManagedFacilities {
		managedTownFacilities = append(managedTownFacilities, ManagedTownFacilitiesArrayObject{
			ActionTime:            int64(obj.ActionTime),
			BuildPointIndex:       int64(obj.BuildPointIndex),
			BuildTime:             int64(obj.BuildTime),
			FacilityId:            int64(obj.FacilityId),
			Level:                 int64(obj.Level),
			ManagedTownFacilityId: int64(obj.ManagedTownFacilityId),
			OpenState:             int64(obj.OpenState),
		})
	}
	var managedTowns []ManagedTownsArrayObject
	for _, obj := range p.ManagedTowns {
		managedTowns = append(managedTowns, ManagedTownsArrayObject{
			GridData:      string(obj.GridData),
			ManagedTownId: int64(obj.ManagedTownId),
		})
	}
	if len(p.ManagedTowns) == 0 {
		managedTowns = make([]ManagedTownsArrayObject, 0)
	}

	var managedWeapons []ManagedWeaponsArrayObject
	for _, obj := range p.ManagedWeapons {
		managedWeapons = append(managedWeapons, ManagedWeaponsArrayObject{
			Exp:             int64(obj.Exp),
			Level:           int64(obj.Level),
			ManagedWeaponId: int64(obj.ManagedWeaponId),
			SkillExp:        int64(obj.SkillExp),
			SkillLevel:      int64(obj.SkillLevel),
			SoldAt:          response.ToSparkleTime(*obj.SoldAt),
			State:           int64(obj.State),
			WeaponId:        int64(obj.WeaponId),
		})
	}
	if len(p.ManagedWeapons) == 0 {
		managedWeapons = make([]ManagedWeaponsArrayObject, 0)
	}

	var offerTitleTypes []OfferTitleTypesArrayObject
	for _, obj := range p.OfferTitleTypes {
		offerTitleTypes = append(offerTitleTypes, OfferTitleTypesArrayObject{
			Category:       int64(obj.Category),
			ContentRoomFlg: int64(obj.ContentRoomFlg),
			OfferIndex:     int64(obj.OfferIndex),
			OfferMaxPoint:  int64(obj.OfferMaxPoint),
			OfferPoint:     int64(obj.OfferPoint),
			Shown:          int64(obj.Shown),
			State:          int64(obj.State),
			SubState:       int64(obj.SubState),
			SubState1:      int64(obj.SubState1),
			SubState2:      int64(obj.SubState2),
			SubState3:      int64(obj.SubState3),
			TitleType:      int64(obj.TitleType),
		})
	}
	var supportCharacters []SupportCharactersArrayObject
	for _, obj := range p.SupportCharacters {
		var managedCharacterIds []int64
		for _, obj := range obj.ManagedCharacterIds {
			managedCharacterIds = append(managedCharacterIds, int64(obj.ManagedCharacterId))
		}
		var managedWeaponIds []int64
		for _, obj := range obj.ManagedWeaponIds {
			managedWeaponIds = append(managedWeaponIds, int64(obj.ManagedWeaponId))
		}
		supportCharacters = append(supportCharacters, SupportCharactersArrayObject{
			Active:              int64(obj.Active),
			ManagedCharacterIds: managedCharacterIds,
			ManagedSupportId:    int64(obj.ManagedSupportId),
			ManagedWeaponIds:    managedWeaponIds,
			Name:                obj.Name,
		})
	}

	var tipIds []int64
	for _, advObj := range p.TipIds {
		tipIds = append(tipIds, int64(advObj.TipId))
	}
	if len(p.TipIds) == 0 {
		tipIds = make([]int64, 0)
	}

	return &GetAllPlayerResponse{
		AdvIds:                   advIds,
		FavoriteMembers:          favoriteMembers,
		FeaturedInformations:     nil,
		FlagPush:                 int64(p.FlagPush),
		FlagStamina:              int64(p.FlagStamina),
		FlagUi:                   int64(p.FlagPush),
		FriendProposedCount:      int64(p.FriendProposedCount),
		IsCloseInfo:              int64(p.IsCloseInfo),
		IsNewProduct:             int64(p.IsNewProduct),
		ItemSummary:              itemSummary,
		LastMemberAdded:          response.ToSparkleTime(p.LastMemberAdded),
		ManagedAbilityBoards:     managedAbilityBoards,
		ManagedBattleParties:     managedBattleParties,
		ManagedCharacters:        managedCharacters,
		ManagedFieldPartyMembers: managedFieldPartyMembers,
		ManagedMasterOrbs:        managedMasterOrbs,
		ManagedNamedTypes:        managedNamedTypes,
		ManagedRoomObjects:       managedRoomObjects,
		ManagedRooms:             managedRooms,
		ManagedTownFacilities:    managedTownFacilities,
		ManagedTowns:             managedTowns,
		ManagedWeapons:           managedWeapons,
		OfferTitleTypes:          offerTitleTypes,
		OrderReceiveId:           int64(p.LatestQuestLogID),
		Player: BasePlayer{
			Age:                  int64(p.Age),
			CharacterLimit:       int64(p.CharacterLimit),
			CharacterWeaponCount: int64(p.CharacterWeaponCount),
			Comment:              p.Comment,
			ContinuousDays:       int64(p.ContinuousDays),
			CreatedAt:            response.ToSparkleTime(p.CreatedAt),
			CurrentAchievementId: int64(p.CurrentAchievementId),
			FacilityLimit:        int64(p.FacilityLimit),
			FacilityLimitCount:   int64(p.FacilityLimitCount),
			FriendLimit:          int64(p.FriendLimit),
			Gold:                 int64(p.Gold),
			Id:                   int64(p.Id),
			IpAddr:               p.IpAddr,
			ItemLimit:            int64(p.ItemLimit),
			Kirara:               int64(p.Kirara),
			KiraraLimit:          int64(p.KiraraLimit),
			LastLoginAt:          response.ToSparkleTime(p.LastLoginAt),
			// TODO: Fix below line can get sparkleTime or nil
			LastPartyAdded:       "0001-01-01T00:00:00",
			Level:                int64(p.Level),
			LevelExp:             int64(p.LevelExp),
			LimitedGem:           int64(p.LimitedGem),
			LoginCount:           int64(p.LoginCount),
			LoginDays:            int64(p.LoginDays),
			LotteryTicket:        int64(p.LotteryTicket),
			MyCode:               p.MyCode,
			Name:                 p.Name,
			PartyCost:            int64(p.PartyCost),
			RecastTime:           int64(p.RecastTime),
			RecastTimeMax:        int64(p.RecastTimeMax),
			RoomObjectLimit:      int64(p.RoomObjectLimit),
			RoomObjectLimitCount: int64(p.RoomObjectLimitCount),
			Stamina:              int64(p.Stamina),
			StaminaMax:           int64(p.StaminaMax),
			StaminaUpdatedAt:     response.ToSparkleTime(p.StaminaUpdatedAt),
			State:                int64(p.State),
			SupportLimit:         int64(p.SupportLimit),
			TotalExp:             int64(p.TotalExp),
			UnlimitedGem:         int64(p.UnlimitedGem),
			UserAgent:            p.UserAgent,
			WeaponLimit:          int64(p.WeaponLimit),
			WeaponLimitCount:     int64(p.WeaponLimitCount),
		},
		PresentCount:      int64(p.PresentCount),
		StepCode:          int64(p.StepCode),
		SupportCharacters: supportCharacters,
		TipIds:            tipIds,
		TrainingCount:     int64(p.TrainingCount),
		// On-going quest only struct
		// TODO: implement save on-going quest
		DropItem:            []DropItemArrayObject{},
		OccurEnemy:          nil,
		QuestData:           nil,
		SupportCharacter:    nil,
		SupportFriend:       nil,
		NewAchievementCount: success.NewAchievementCount,
		ResultCode:          success.ResultCode,
		ResultMessage:       success.ResultMessage,
		ServerTime:          success.ServerTime,
		ServerVersion:       success.ServerVersion,
	}
}
