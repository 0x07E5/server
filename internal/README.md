## internal

This is core implementation directory.
Welcome to DDD architecture!

### Base folder structure

### Current

- interfaces
- application/service or application/usecase
- domain/model+value
- domain/repository(infrastructure/persistence)

### Future

- controller
  - controller/model
- registry/usecase+adapter
- application/usecase
- application/service
- (application/schemas as DTO)
- domain/model+value+factory
- domain/repository/persistence
  - (infrastructure/persistence)
  - (infrastructure/persistence/dto)
- domain/repository/adapter
  - (infrastructure/adapter)
