package value

import "errors"

type MissionSegType uint8

const (
	// Login x times / clear all mission / clear all beginner mission
	MissionSegTypeServer MissionSegType = iota
	// Clear x times / clear specific quest
	MissionSegTypeQuestClear
	// Tap x times / build facility
	MissionSegTypeTown
	// Tap character / Visit room / Put room item
	MissionSegTypeRoom
	// Evolution / LimitBreak / Create weapon / Create a deck / Create a support deck
	MissionSegTypeCharacterEnhance
	MissionSegTypeCharacterRelationship
	// Start training x times
	MissionSegTypeTraining
	// Trading x times
	MissionSegTypeTrade
)

var ErrInvalidMissionSegType = errors.New("invalid MissionSegType")

func NewMissionSegType(v uint8) (MissionSegType, error) {
	if v > uint8(MissionSegTypeTrade) {
		return MissionSegTypeServer, ErrInvalidMissionSegType
	}
	return MissionSegType(v), nil
}
