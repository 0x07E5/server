package value

import "errors"

type ItemCategory uint16

const (
	// Seeds
	ItemCategorySmallSeed      ItemCategory = 0
	ItemCategoryNormalSeed     ItemCategory = 1
	ItemCategoryLargeSeed      ItemCategory = 2
	ItemCategoryExtraLargeSeed ItemCategory = 3
	// 星華 (Unused category)
	ItemCategoryContentLimitedStarflower ItemCategory = 4
	// Evolution Jewels
	ItemCategorySmallEvolutionJewel      ItemCategory = 100
	ItemCategoryNormalEvolutionJewel     ItemCategory = 101
	ItemCategoryLargeEvolutionJewel      ItemCategory = 102
	ItemCategoryExtraLargeEvolutionJewel ItemCategory = 103
	ItemCategorySuperEvolutionJewel      ItemCategory = 104
	// Statues
	ItemCategoryBronzeStatue ItemCategory = 112
	ItemCategorySilverStatue ItemCategory = 113
	ItemCategoryGoldStatue   ItemCategory = 114
	// コンテンツ限定超進化珠 (Unused category)
	ItemCategoryContentLimitedSuperEvolutionJewel ItemCategory = 124
	// Buds, fruits, and Star Crystals
	ItemCategoryLimitBreakStar3               ItemCategory = 202
	ItemCategoryLimitBreakStar4               ItemCategory = 203
	ItemCategoryLimitBreakStar5               ItemCategory = 204
	ItemCategoryLimitBreakStar5ContentLimited ItemCategory = 214
	// Symbols, crests, and weapon materials
	ItemCategorySymbol                 ItemCategory = 300
	ItemCategoryCrest                  ItemCategory = 301
	ItemCategoryChapter1WeaponMaterial ItemCategory = 311
	ItemCategoryChapter2WeaponMaterial ItemCategory = 312
	ItemCategoryChapter3WeaponMaterial ItemCategory = 313
	ItemCategoryChapter4WeaponMaterial ItemCategory = 314
	ItemCategoryChapter5WeaponMaterial ItemCategory = 315
	ItemCategoryChapter6WeaponMaterial ItemCategory = 316
	ItemCategoryChapter7WeaponMaterial ItemCategory = 317
	ItemCategoryChapter8WeaponMaterial ItemCategory = 318
	// Special items, summon tickets, exchange items, stamina recovery items
	ItemCategorySpecial           ItemCategory = 404
	ItemCategorySummonTicket      ItemCategory = 405
	ItemCategoryExchangeItem      ItemCategory = 406
	ItemCategoryStaminaItem       ItemCategory = 407
	ItemCategoryEtherium          ItemCategory = 408
	ItemCategoryChanceUpKeyHolder ItemCategory = 409
	ItemCategorySkillUpPowder     ItemCategory = 410
)

var ErrInvalidItemCategory = errors.New("invalid ItemCategory")

func NewItemCategory(v uint16) (ItemCategory, error) {
	if v > uint16(ItemCategorySkillUpPowder) {
		return ItemCategorySmallSeed, ErrInvalidItemCategory
	}
	return ItemCategory(v), nil
}
