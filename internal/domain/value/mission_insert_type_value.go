package value

import "errors"

type MissionInsertType uint8

const (
	MissionInsertTypeTutorial MissionInsertType = iota
	MissionInsertTypeDaily
	MissionInsertTypeWeekly
	MissionInsertTypeRankUp
	MissionInsertTypeCharacterRelationship
	MissionInsertTypeCharacterEvolution
	MissionInsertTypeEvent
)

var ErrInvalidMissionInsertType = errors.New("invalid MissionInsertType")

func NewMissionInsertType(v uint8) (MissionInsertType, error) {
	if v > uint8(MissionInsertTypeEvent) {
		return MissionInsertTypeTutorial, ErrInvalidMissionInsertType
	}
	return MissionInsertType(v), nil
}
