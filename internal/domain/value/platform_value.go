package value

import "errors"

type Platform uint8

const (
	PlatformIOS Platform = iota + 1
	PlatformAndroid
)

var ErrInvalidPlatform = errors.New("invalid Platform")

func NewPlatform(v uint8) (Platform, error) {
	if v < uint8(PlatformIOS) || v > uint8(PlatformAndroid) {
		return PlatformIOS, ErrInvalidPlatform
	}
	return Platform(v), nil
}
