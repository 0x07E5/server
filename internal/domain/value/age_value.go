package value

import "errors"

type Age uint8

const (
	AgeUnknown Age = iota
	AgeChild
	AgeStudent
	AgeAdult
)

var ErrInvalidAge = errors.New("invalid age")

func NewAge(value uint8) (Age, error) {
	if value > uint8(AgeAdult) {
		return AgeUnknown, ErrInvalidAge
	}
	return Age(value), nil
}

func NewAgeFromNumber(age int) Age {
	if age == -1 {
		return AgeUnknown
	}
	if age < 17 {
		return AgeChild
	}
	if age < 20 {
		return AgeStudent
	}
	return AgeAdult
}

func (a *Age) Update(age Age) Age {
	return age
}
