package value_room

import "errors"

type RoomObjectDirection uint8

const (
	RoomObjectDirectionUnknown0 RoomObjectDirection = iota
	RoomObjectDirectionUnknown1
	RoomObjectDirectionUnknown2
	RoomObjectDirectionUnknown3
)

var ErrInvalidRoomObjectDirection = errors.New("invalid RoomObjectDirection. RoomObjectDirection must be in 0 to 3")

func NewRoomObjectDirection(value uint8) (RoomObjectDirection, error) {
	if value > uint8(RoomObjectDirectionUnknown3) {
		return 0, ErrInvalidRoomObjectDirection
	}
	return RoomObjectDirection(value), nil
}
