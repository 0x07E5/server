package value

type GachaResult struct {
	CharacterId CharacterId
	ItemId      int32
	ItemAmount  uint32
}
