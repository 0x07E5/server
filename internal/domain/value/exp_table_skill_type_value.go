package value

import "errors"

type ExpTableSkillType uint8

const (
	// クラススキル・とっておき・ぶきスキル
	ExpTableSkillTypeNormal ExpTableSkillType = iota
	// 不明 (なんのスキルの経験値テーブル?)
	ExpTableSkillTypeSpecial
	// 専用武器
	ExpTableSkillTypeWeapon
)

var ErrInvalidExpTableSkillType = errors.New("invalid ExpTableSkillType")

func NewExpTableSkillType(value uint8) (ExpTableSkillType, error) {
	if value > uint8(ExpTableSkillTypeWeapon) {
		return 0, ErrInvalidExpTableSkillType
	}
	return ExpTableSkillType(value), nil
}
