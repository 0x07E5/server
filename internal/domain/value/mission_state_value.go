package value

import "errors"

type MissionState uint8

const (
	MissionStateNotCleared MissionState = 0
	MissionStateCleared    MissionState = 99
)

var ErrInvalidMissionState = errors.New("invalid MissionState")

func NewMissionState(v uint8) (MissionState, error) {
	if v != uint8(MissionStateNotCleared) && v != uint8(MissionStateCleared) {
		return MissionStateNotCleared, ErrInvalidMissionState
	}
	return MissionState(v), nil
}
