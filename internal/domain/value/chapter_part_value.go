package value

import "errors"

type ChapterPart uint8

const (
	ChapterPart1 ChapterPart = iota
	ChapterPart2
)

var ErrInvalidChapterPart = errors.New("invalid chapter part")

func NewChapterPart(value uint8) (ChapterPart, error) {
	if value > 1 {
		return ChapterPart(0), ErrInvalidChapterPart
	}
	return ChapterPart(value), nil
}
