package value

import "errors"

type FacilityCategory uint8

const (
	FacilityCategoryRoom           FacilityCategory = 0
	FacilityCategoryWeaponMaterial FacilityCategory = 1
	FacilityCategoryGold           FacilityCategory = 2
	FacilityCategoryGeneral        FacilityCategory = 4
	FacilityCategoryClass          FacilityCategory = 5
	FacilityCategoryTitle          FacilityCategory = 6
	FacilityCategorySystem         FacilityCategory = 7
)

var ErrInvalidFacilityCategory = errors.New("invalid FacilityCategory")

func NewFacilityCategory(v uint8) (FacilityCategory, error) {
	if v > uint8(FacilityCategorySystem) {
		return FacilityCategoryRoom, ErrInvalidFacilityCategory
	}
	return FacilityCategory(v), nil
}
