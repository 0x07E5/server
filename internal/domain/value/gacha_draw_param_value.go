package value

// Gacha draw request param
type GachaDrawParam struct {
	DrawType            GachaDrawType
	IsChanceUp          bool
	Is10Roll            bool
	SelectedCharacterId uint64
}
