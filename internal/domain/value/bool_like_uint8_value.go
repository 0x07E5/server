package value

// Since kirafan API uses 0 and 1 as boolean in many cases
type BoolLikeUInt8 uint8

const (
	BoolLikeUIntFalse BoolLikeUInt8 = iota
	BoolLikeUIntTrue
)

func NewBoolLikeUint8(value uint8) (BoolLikeUInt8, error) {
	if value > uint8(BoolLikeUIntTrue) {
		return 0, ErrInvalidCharacterRarity
	}
	return BoolLikeUInt8(value), nil
}
