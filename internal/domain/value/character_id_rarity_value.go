package value

type CharacterIdRarity uint8

const (
	CharacterIdRarityStar3 CharacterIdRarity = iota
	CharacterIdRarityStar4
	CharacterIdRarityStar5
)
