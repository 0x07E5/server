package model_user

import "time"

func NewUserGacha(userId uint, gachaId uint) UserGacha {
	now := time.Now()
	return UserGacha{
		CreatedAt:          now,
		UpdatedAt:          now,
		DeletedAt:          nil,
		Gem10CurrentStep:   1,
		PlayerDrawPoint:    0,
		Gem10Daily:         0,
		Gem10Total:         0,
		Gem1Daily:          0,
		Gem1Total:          0,
		UGem1Daily:         0,
		UGem1Total:         0,
		ItemDaily:          0,
		ItemTotal:          0,
		Gem1FreeDrawCount:  0,
		Gem10FreeDrawCount: 0,
		GachaId:            gachaId,
		UserId:             userId,
	}
}
