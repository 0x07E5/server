package model_user

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

type ManagedNamedType struct {
	ManagedNamedTypeId uint `gorm:"primarykey"`
	// Foreign Key TODO: add relation later
	UserId    uint
	NamedType uint16
	Level     uint8
	Exp       uint32
	TitleType value.TitleType
	// Unknown (default 0)
	FriendshipExpTableId uint8
}

func (m *ManagedNamedType) AddExp(amount uint32) {
	m.Exp += amount
}

func (m *ManagedNamedType) UpdateLevel(level uint8) {
	m.Level = calc.MaxUint8(m.Level, level)
}
