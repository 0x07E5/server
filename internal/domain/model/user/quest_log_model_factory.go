package model_user

import (
	"errors"
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

var ErrNewQuestLogInvalidManagedBattlePartyId = errors.New("invalid managed battle party id")

func NewQuestLog(
	user User,
	managedBattlePartyId uint,
	questId uint,
	questData string,
	waveData *string,
	dropData *string,
	waveCount uint8,
) (QuestLog, error) {
	now := time.Now()
	var managedBattleParty *ManagedBattleParty
	for _, battleParty := range user.ManagedBattleParties {
		if uint(battleParty.ManagedBattlePartyId) == managedBattlePartyId {
			managedBattleParty = &battleParty
			break
		}
	}
	if managedBattleParty == nil {
		return QuestLog{}, ErrNewQuestLogInvalidManagedBattlePartyId
	}

	questLog := QuestLog{
		UserId:      user.Id,
		QuestId:     questId,
		CreatedAt:   now,
		UpdatedAt:   now,
		QuestData:   questData,
		WaveData:    waveData,
		DropData:    dropData,
		ClearRank:   value.ClearRankNone,
		IsRead:      0,
		CurrentWave: 0,
		TotalWave:   waveCount,
		Characters:  []QuestLogCharacter{},
		Support:     nil,
	}
	return questLog, nil
}
