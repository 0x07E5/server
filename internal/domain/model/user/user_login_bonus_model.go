package model_user

import (
	"time"

	model_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/model/login_bonus"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

// Moved from login_bonus model, dayIndex depends per a user
type UserLoginBonus struct {
	UserLoginBonusId uint `gorm:"primary_key"`
	CreatedAt        time.Time
	UpdatedAt        time.Time
	DeletedAt        *time.Time `sql:"index"`

	LoginBonusDayIndex value.LoginBonusDayIndex

	// foreignKey
	LoginBonusId uint
	LoginBonus   model_login_bonus.LoginBonus
	// foreignKey
	UserId uint
	User   User `gorm:"PRELOAD:false"`
}
