package model_user

import "gorm.io/gorm"

// Showed tip ids (not tipId since it conflicts to type)
type ShownTipId struct {
	gorm.Model
	// Foreign key
	UserId uint
	TipId  uint32
}
