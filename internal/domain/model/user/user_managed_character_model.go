package model_user

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	"gitlab.com/kirafan/sparkle/server/pkg/calc"
)

type ManagedCharacter struct {
	ManagedCharacterId int `gorm:"primarykey"`
	// ForeignKey
	PlayerId uint
	// レベル現在値 (1~100)
	Level uint8
	// レベル最大値 (30~100)
	LevelLimit uint8
	// 経験値
	Exp uint64
	// 限界突破回数 (0~4)
	LevelBreak uint8
	// 重複回数
	DuplicatedCount uint8
	// 覚醒レベル (0~5)
	ArousalLevel uint8
	// とっておき 現在レベル (1~35)
	SkillLevel1 uint8
	// とっておき 最大レベル (5~35)
	SkillLevelLimit1 uint8
	// とっておき 使用回数 (0~)
	SkillExp1 uint32
	// スキル1 現在レベル (1~25)
	SkillLevel2 uint8
	// スキル1 最大レベル (5~25)
	SkillLevelLimit2 uint8
	// スキル1 使用回数 (0~)
	SkillExp2 uint32
	// スキル2 現在レベル (1~)
	SkillLevel3 uint8
	// スキル2 最大レベル (5~25)
	SkillLevelLimit3 uint8
	// スキル2 使用回数 (0~)
	SkillExp3 uint32
	// isNew (0: un-shown / 1: shown)
	Shown uint8
	// CardId
	CharacterId value.CharacterId
	// SD displayId
	ViewCharacterId uint32
}

type SkillType uint8

const (
	SkillType1 SkillType = iota
	SkillType2
	SkillType3
)

func (m *ManagedCharacter) UpdateLevel(level uint8) {
	newLevel := calc.MaxUint8(m.Level, level)
	m.Level = calc.MinUint8(newLevel, m.LevelLimit)
}

func (m *ManagedCharacter) AddExp(exp uint64) {
	m.Exp += exp
}

func (m *ManagedCharacter) UpdateSkillLevel(level uint8, skillType SkillType) {
	switch skillType {
	case SkillType1:
		newLevel := calc.MaxUint8(m.SkillLevel1, level)
		m.SkillLevel1 = calc.MinUint8(newLevel, m.SkillLevelLimit1)
	case SkillType2:
		newLevel := calc.MaxUint8(m.SkillLevel2, level)
		m.SkillLevel2 = calc.MinUint8(newLevel, m.SkillLevelLimit2)
	case SkillType3:
		newLevel := calc.MaxUint8(m.SkillLevel3, level)
		m.SkillLevel3 = calc.MinUint8(newLevel, m.SkillLevelLimit3)
	default:
	}
}

func (m *ManagedCharacter) AddSkillExp(exp uint32, skillType SkillType) {
	switch skillType {
	case SkillType1:
		m.SkillExp1 += exp
	case SkillType2:
		m.SkillExp2 += exp
	case SkillType3:
		m.SkillExp3 += exp
	}
}

// Add duplicated count and increase ArousalLevel if needed
func (m *ManagedCharacter) IncreaseDuplicatedCount() bool {
	m.DuplicatedCount++
	var levelUpCounts = map[value.CharacterIdRarity][]uint8{
		value.CharacterIdRarityStar3: {1, 6, 17},
		value.CharacterIdRarityStar4: {1, 4, 7, 11},
		value.CharacterIdRarityStar5: {1, 2, 3, 4, 5},
	}
	var skillLevel1UpCounts = map[value.CharacterIdRarity][]uint8{
		value.CharacterIdRarityStar3: {2, 2, 2},
		value.CharacterIdRarityStar4: {2, 1, 1, 1},
		value.CharacterIdRarityStar5: {2, 2, 2, 2, 2},
	}
	if len(levelUpCounts) != len(skillLevel1UpCounts) {
		panic("levelUpCounts and skillLevel1UpCounts must have the same length")
	}
	rarity := m.CharacterId.GetRarity()
	if m.ArousalLevel < uint8(len(levelUpCounts[rarity])) && m.DuplicatedCount == levelUpCounts[rarity][m.ArousalLevel] {
		m.ArousalLevel++
		m.SkillLevelLimit1 += skillLevel1UpCounts[rarity][m.ArousalLevel-1]
	}
	return false
}
