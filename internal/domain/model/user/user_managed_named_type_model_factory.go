package model_user

import (
	"errors"

	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

func newManagedNamedType(userId uint, namedType uint16, titleType value.TitleType) (ManagedNamedType, error) {
	if namedType > 247 {
		return ManagedNamedType{}, errors.New("namedType must be between 0 and 247")
	}
	if titleType > 37 {
		return ManagedNamedType{}, errors.New("titleType must be between 0 and 37")
	}
	managedNamedType := ManagedNamedType{
		UserId:               userId,
		NamedType:            namedType,
		Level:                1,
		Exp:                  0,
		TitleType:            titleType,
		FriendshipExpTableId: 0,
	}
	return managedNamedType, nil
}
