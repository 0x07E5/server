package model_user

import "gitlab.com/kirafan/sparkle/server/internal/domain/value"

func newManagedCharacter(userId uint, characterId value.CharacterId) ManagedCharacter {
	managedCharacter := ManagedCharacter{
		PlayerId:         userId,
		CharacterId:      characterId,
		ViewCharacterId:  uint32(characterId),
		Level:            1,
		Exp:              0,
		LevelBreak:       0,
		DuplicatedCount:  0,
		ArousalLevel:     0,
		SkillLevel1:      1,
		SkillLevel2:      1,
		SkillLevel3:      1,
		SkillExp1:        0,
		SkillExp2:        0,
		SkillExp3:        0,
		SkillLevelLimit1: 5,
		SkillLevelLimit2: 5,
		SkillLevelLimit3: 5,
		Shown:            0,
	}
	characterRarity := characterId.GetRarity()
	switch characterRarity {
	case value.CharacterIdRarityStar3:
		managedCharacter.LevelLimit = 30
	case value.CharacterIdRarityStar4:
		managedCharacter.LevelLimit = 40
	case value.CharacterIdRarityStar5:
		managedCharacter.LevelLimit = 50
	}
	return managedCharacter
}
