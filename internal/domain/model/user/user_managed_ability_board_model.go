package model_user

import "gorm.io/gorm"

type ManagedAbilityBoard struct {
	ManagedAbilityBoardId uint `gorm:"primarykey"`
	// Foreign Key
	UserId uint
	// Foreign Key (TODO: Add the key to migration)
	ManagedCharacterId int
	AbilityBoardId     uint32
	EquipItemIds       []ManagedAbilityBoardEquipItemId
}

type ManagedAbilityBoardEquipItemId struct {
	gorm.Model
	// Foreign Key (TODO: Add the key to migration)
	ItemId uint32
	// Foreign Key (TODO: Add the key to migration)
	ManagedAbilityBoardId uint
}
