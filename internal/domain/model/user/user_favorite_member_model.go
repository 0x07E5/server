package model_user

import "gorm.io/gorm"

// Favorites at player card
type FavoriteMember struct {
	gorm.Model
	// Foreign key
	UserId uint
	// Foreign key
	ManagedCharacterId int
	FavoriteIndex      uint8  `json:"favoriteIndex"`
	CharacterId        uint32 `json:"characterId"`
	ArousalLevel       uint8  `json:"arousalLevel"`
}
