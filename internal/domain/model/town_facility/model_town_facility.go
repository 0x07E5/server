package model_town_facility

import "gitlab.com/kirafan/sparkle/server/internal/domain/value"

type TownFacility struct {
	// NOTE: It accepts nil Since facilityId can be 0
	// https://stackoverflow.com/questions/54523765/update-with-0-value-using-gorm
	FacilityId       *uint32 `gorm:"primary_key"`
	FacilityCategory value.FacilityCategory
	Name             string
	Detail           string
	MaxLevel         uint8
	MaxNum           uint8
	TitleType        value.TitleType
	ClassType        value.ClassType
	BuildTimeType    value.FacilityBuildTimeType
	LevelUpListId    value.LevelUpListId
	IsTutorial       value.BoolLikeUInt8
}
