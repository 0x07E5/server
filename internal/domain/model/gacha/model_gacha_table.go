package model_gacha

import "gitlab.com/kirafan/sparkle/server/internal/domain/value"

type GachaTable struct {
	GachaTableId uint `gorm:"primary_key"`
	CharacterId  uint64
	Pickup       bool
	Rarity       value.CharacterRarity
	// Foreign key
	GachaId uint
}
