package model_gacha

import (
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type GachaDailyFree struct {
	GachaDailyFreeId uint `gorm:"primary_key"`
	GachaId          uint32
	DrawType         value.GachaDrawType
	StartAt          time.Time
	EndAt            time.Time
}
