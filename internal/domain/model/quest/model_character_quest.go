package model_quest

type CharacterQuest struct {
	CharacterQuestId uint `gorm:"primaryKey"`
	// Foreign key
	QuestId     uint
	Quest       Quest
	CharacterId uint
}
