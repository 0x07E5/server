package model_quest

import "gitlab.com/kirafan/sparkle/server/internal/domain/value"

type OfferQuest struct {
	OfferQuestId uint `gorm:"primaryKey"`
	OfferId      uint
	// Foreign key
	QuestId   uint
	Quest     Quest
	Category  value.QuestCategoryType
	TitleType int64
}
