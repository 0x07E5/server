package model_quest

import (
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type QuestStaminaReduction struct {
	Id            uint `gorm:"primaryKey"`
	QuestCategory value.QuestCategoryType
	Difficulty    uint8
	TargetId      uint64
	StartAt       time.Time
	EndAt         time.Time
}
