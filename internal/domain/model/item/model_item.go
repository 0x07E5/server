package model_item

import "gitlab.com/kirafan/sparkle/server/internal/domain/value"

type Item struct {
	ItemId       uint `gorm:"primary_key"`
	ItemSortId   uint
	Name         string
	Description  string
	Category     value.ItemCategory
	Type         value.ItemType
	Rare         value.ItemRare
	SaleAmount   int32
	TypeArg1     int32
	TypeArg2     int32
	TypeArg3     int32
	TypeArg4     int32
	Appeal       value.BoolLikeUInt8
	IsEventBonus bool
}
