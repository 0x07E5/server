package model_present

import (
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type Present struct {
	PresentId uint `gorm:"primary_key"`
	CreatedAt time.Time
	// Ignore deadline if this value is true (why drecom)
	DeadlineFlag bool
	// Basically "<item name> x<amount>" text should be at the title
	Title string
	// The reason of this present (Ex. mission reward, login bonus, gift, etc.)
	Message string
	// Template values for this present ( if there are lot combinations like mission reward this can be leave 0)
	Type     value.PresentType
	Amount   int64
	ObjectId int64 // ItemId or CharacterId
	// Mystery... (default nil)
	Options *string
	Source  value.PresentSource
	// Custom field to handle present system
	// This field will used as database key to find next mission
	PresentInsertType value.PresentInsertType
}
