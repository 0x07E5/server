package model_mission

import "gitlab.com/kirafan/sparkle/server/internal/domain/value"

type MissionReward struct {
	Id uint `gorm:"primary_key"`
	// Foreign key
	MissionId  uint
	RewardType value.MissionRewardType
	// ItemId or -1
	RewardNo int64
	// Reward count to obtain (must be big because of coin is 64bit)
	RewardNum int64
}
