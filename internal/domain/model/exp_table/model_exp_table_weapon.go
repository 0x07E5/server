package model_exp_table

import "gitlab.com/kirafan/sparkle/server/internal/domain/value"

type ExpTableWeapon struct {
	ExpTableWeaponId    uint `gorm:"primaryKey"`
	WeaponType          value.ExpTableWeaponType
	Level               uint16
	NextExp             uint32
	TotalExp            uint32
	RequiredCoinPerItem uint16
}
