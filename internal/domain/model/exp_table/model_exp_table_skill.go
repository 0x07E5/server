package model_exp_table

import "gitlab.com/kirafan/sparkle/server/internal/domain/value"

type ExpTableSkill struct {
	ExpTableSkillId uint `gorm:"primaryKey"`
	SkillType       value.ExpTableSkillType
	Level           uint8
	NextExp         uint32
	TotalExp        uint32
}
