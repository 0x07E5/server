package model_login_bonus

import (
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type LoginBonus struct {
	BonusId   uint            `gorm:"primaryKey"`
	BonusDays []LoginBonusDay `gorm:"foreignKey:BonusId"`
	ImageId   value.LoginBonusImageId
	Type      value.LoginBonusType
	// Internal request accept time range
	StartAt time.Time
	EndAt   time.Time
}
