package model_login_bonus

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type LoginBonusDayBonusItem struct {
	BonusDayBonusItemId uint `gorm:"primaryKey"`
	// foreign key
	BonusDayId value.LoginBonusDayIndex
	Type       value.LoginBonusDayBonusItemPresentType
	ObjId      int32
	Amount     uint32
	// messageMakeId is dead parameter
}
