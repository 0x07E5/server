package model_login_bonus

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type LoginBonusDay struct {
	BonusDayId value.LoginBonusDayIndex `gorm:"primaryKey"`
	// foreign key
	BonusId uint
	IconId  value.LoginBonusDayIconId
	// It must be 3 fields
	BonusItems []LoginBonusDayBonusItem `gorm:"foreignKey:BonusDayId"`
}
