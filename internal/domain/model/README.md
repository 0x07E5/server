## domain/model

This is the domain model or entity at DDD.
They have gorm model form.

main <-> middleware <-> router(interfaces) <-> usecase <-> repository <-> **model(gorm)** <-> db(persistence)
