package model_item_table

import "gitlab.com/kirafan/sparkle/server/internal/domain/value"

type ItemTableTownFacility struct {
	LevelTableFacilityID uint `gorm:"primary_key"`
	ItemNo               uint32
	Category             value.ItemTableTownFacilityCategory
	ObjectID             int64
	ObjectCount          uint32
}
