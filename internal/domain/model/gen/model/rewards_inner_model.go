package model

type RewardsInner struct {
	ContentType int64 `json:"contentType,omitempty"`

	GrantType int64 `json:"grantType,omitempty"`

	ItemAmount int64 `json:"itemAmount,omitempty"`

	ItemId int64 `json:"itemId,omitempty"`

	PremiumId int64 `json:"premiumId,omitempty"`
}
