package model

type DrawPlayerChestResponseChest struct {
	BannerName string

	BgName string

	CostItemAmount int64

	CostItemId int64

	CurrentStep int64

	CurrentStock int64

	DispEndAt string

	DispStartAt string

	EnableReset int64

	EndAt string

	EventType int64

	Id int64

	MaxStep int64

	Name string

	ResetChestPrizes []ResetChestPrizesArrayObject

	StartAt string

	TotalStock int64

	TradeGroupId int64

	TutorialTipsId int64
}
