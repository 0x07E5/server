package model

type QuestStaminaReductionsArrayObject struct {
	Difficulty int64

	EndAt string

	QuestCategory int64

	StartAt string

	TargetId int64
}
