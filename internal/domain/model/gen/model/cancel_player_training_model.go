package model

type CancelPlayerTrainingResponse struct {
	Player BasePlayer

	SlotInfo []SlotInfoArrayObject

	TrainingInfo []TrainingInfoArrayObject
}
