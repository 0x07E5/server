package model

type OfferTitleTypesArrayObject struct {
	Category int64

	ContentRoomFlg int64

	OfferIndex int64

	OfferMaxPoint int64

	OfferPoint int64

	Shown int64

	State int64

	SubState int64

	SubState1 int64

	SubState2 int64

	SubState3 int64

	TitleType int64
}
