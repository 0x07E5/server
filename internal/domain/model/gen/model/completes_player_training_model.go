package model

type CompletesPlayerTrainingResponse struct {
	ItemSummary []ItemSummaryArrayObject

	ManagedCharacters []ManagedCharactersArrayObject

	ManagedNamedTypes []ManagedNamedTypesArrayObject

	Player BasePlayer

	SlotInfo []SlotInfoArrayObject

	TrainingCompleteRewards []TrainingCompleteRewardsArrayObject

	TrainingInfo []TrainingInfoArrayObject
}
