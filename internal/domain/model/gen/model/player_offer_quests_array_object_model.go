package model

type PlayerOfferQuestsArrayObject struct {
	Category int64

	ClearRank int64

	IsOrder int64

	IsRead int64

	OfferId int64

	Quest PlayerOfferQuestsArrayObjectQuest

	TitleType int64
}
