package model

type UpgradeSpherePlayerAbilityResponse struct {
	ItemSummary []ItemSummaryArrayObject

	Player BasePlayer

	PlayerAbilityBoards []PlayerAbilityBoardsArrayObject
}
