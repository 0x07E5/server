package model

type ReapDropPlayerScheduleResponse struct {
	ItemSUmmary []ItemSummaryArrayObject

	ManagedNamedTypes []ManagedNamedTypesArrayObject

	Player BasePlayer
}
