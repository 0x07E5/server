package model

type AddPlayerStaminaResponse struct {
	ItemSummary []ItemSummaryArrayObject

	Player BasePlayer
}
