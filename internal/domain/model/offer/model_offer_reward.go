package model_offer

import "gitlab.com/kirafan/sparkle/server/internal/domain/value"

type OfferReward struct {
	// Database primary key
	Id uint `gorm:"primary_key"`

	// Foreign key
	OfferId uint

	// Same as present type
	ContentType value.PresentType

	// ItemID, MasterOrbID, RoomObjectID, or AchievementID, otherwise -1
	ContentId int64

	// ItemName RoomObjectName, or AchievementName, otherwise ""
	ContentName string

	// CoinCount, ItemCount, MasterOrbLv, otherwise 1
	ContentAmount int64
}
