package model_offer

import "gitlab.com/kirafan/sparkle/server/internal/domain/value"

type Offer struct {
	// Offer Type ID
	OfferId uint `gorm:"primary_key"`
	// CreaCraft / CreaCraftBonus / CreamateCommunity
	Category value.OfferCategory
	// Display index at list (Smaller value goes front, Bigger value goes back)
	OfferIndex int16
	// Source title type (Ex. TitleTypeGochiusa)
	TitleType value.TitleType
	// Offer state (Ex. OfferStateNotStarted)
	State value.OfferState

	// Offer title name (Ex. The cutest girl in the world)
	Title *string
	// Offer description (Ex. she is cute! mofu-mofu!)
	Body *string
	// Offer client name (Ex. Dosugamea)
	ClientName *string

	// Condition type to complete this offer (Ex. OfferFuncTypeCreaAmount )
	FuncType value.OfferFuncType
	// Same value as transitQuestId (???)
	FuncId int64
	// Required crea value to begin this offer (offerPoint moved to  userModel)
	OfferMaxPoint int64
	// Required progress value to complete (Ex. 3 of 1/3 ) (progress moved to userModel)
	MaxProgress int64
	// Required quest id to complete
	TransitQuestId int64
	// Required quest name to complete
	TransitQuestName string

	// Rewards when complete this offer
	OfferRewards []OfferReward
}
