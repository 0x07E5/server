package model_version

import (
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
	"gorm.io/gorm"
)

type Version struct {
	gorm.Model
	Platform            value.Platform
	Version             string
	AbVer               uint32
	ResourceVersionHash string `gorm:"column:resource_version_hash;type:varchar(32);unique_index"`
}

func NewVersion(platform value.Platform, version string, abVer uint32, resourceVersionHash string) Version {
	return Version{
		Platform:            platform,
		Version:             version,
		AbVer:               abVer,
		ResourceVersionHash: resourceVersionHash,
	}
}
