package model_room_object

import (
	"time"

	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type RoomObject struct {
	// internal
	Id uint `gorm:"primary_key"`
	// This value is **Not** unique !!!
	RoomObjectId uint32
	//Default -1
	//  Amount means price
	BargainBuyAmount int32
	// if none : 0001-01-01T00:00:00
	BargainEndAt time.Time
	BargainFlag  value.BoolLikeUInt8
	// if none : 0001-01-01T00:00:00
	BargainStartAt time.Time
	//  Amount means price
	BuyAmount uint32
	// if none : 2099-12-31T00:00:00
	DispEndAt time.Time
	// default : 2017-01-01T00:00:00
	DispStartAt time.Time
	IsSaleable  bool
	Name        string
	// Maximum number of RoomObject
	ObjectLimit uint8
	// Amount means price
	SaleAmount int32
	ShopFlag   value.BoolLikeUInt8
	Type       value.RoomObjectType
}
