package model_named_type

import "gitlab.com/kirafan/sparkle/server/internal/domain/value"

type NamedType struct {
	NamedType          uint `gorm:"primaryKey;autoIncrement:false"`
	TitleType          value.TitleType
	NickName           string
	FullName           string
	DropItemKey        uint16
	PrimaryCharacterId uint32
	// TODO: Find out what this is
	BattleWinId uint8
	// Maybe dead parameter?
	FriendshipTableId uint8
	PersonalityType   uint8
}
