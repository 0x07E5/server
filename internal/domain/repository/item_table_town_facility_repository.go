package repository

import (
	model_item_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/item_table"
)

type ItemTableTownFacilityRepository interface {
	FindItemTableTownFacility(itemNo uint32) ([]*model_item_table.ItemTableTownFacility, error)
}
