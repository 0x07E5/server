package repository

import (
	model_login_bonus "gitlab.com/kirafan/sparkle/server/internal/domain/model/login_bonus"
)

type LoginBonusRepository interface {
	FindAvailableLoginBonuses() ([]*model_login_bonus.LoginBonus, error)
}
