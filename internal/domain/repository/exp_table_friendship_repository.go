package repository

import (
	model_exp_table "gitlab.com/kirafan/sparkle/server/internal/domain/model/exp_table"
)

type ExpTableFriendshipRepository interface {
	FindExpTableFriendship(query *model_exp_table.ExpTableFriendship, criteria map[string]interface{}) (*model_exp_table.ExpTableFriendship, error)
	FindExpTableFriendships(query *model_exp_table.ExpTableFriendship, criteria map[string]interface{}) ([]*model_exp_table.ExpTableFriendship, error)
}
