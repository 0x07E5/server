package repository

import (
	model_item "gitlab.com/kirafan/sparkle/server/internal/domain/model/item"
)

type ItemRepository interface {
	FindItem(query *model_item.Item, criteria map[string]interface{}, associations *[]string) (*model_item.Item, error)
	FindItems(query *model_item.Item, criteria map[string]interface{}, associations *[]string) ([]*model_item.Item, error)
}
