package repository

import (
	model_schedule "gitlab.com/kirafan/sparkle/server/internal/domain/model/schedule"
	"gitlab.com/kirafan/sparkle/server/internal/domain/value"
)

type ScheduleRepository interface {
	Create(characterIds []value.CharacterId, gridData string) ([]*model_schedule.Schedule, error)
	Refresh(characterSchedules []string) ([]string, error)
}
