package repository

type LoggerRepository interface {
	Info(message string)
	Warn(message string)
	Error(message string)
	Debug(message string)
}
