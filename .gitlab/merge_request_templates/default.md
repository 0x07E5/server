## Summary

<!-- Provide a brief summary of the changes you have made in this pull request.-->

## Related Issues

<!-- Include a list of related issues that this pull request addresses or closes. For example: "Closes #123" or "Fixes #456".-->

## Changes Made

<!-- Provide a detailed description of the changes you have made in this pull request, including any new features, enhancements, or bugfixes. Be sure to explain the reasoning behind your changes and how they address the related issues.-->

## Testing

<!-- Describe the testing you have done to verify that your changes work as intended. Include any relevant test cases or steps.-->

## Checklist

- [ ] No extra unnecessary files are included.
- [ ] No extra fmt.Println() or fmt.Printf() statements are present.
- [ ] Test codes are included, and works.
- [ ] The game client can accept the response (optional)

## Additional Information

<!-- Provide any additional information that is not covered by the other sections.-->
