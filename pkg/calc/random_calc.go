package calc

import (
	"math/rand"
	"time"
)

func CumulativeRandomChoice(weights []int64, seed *int64) int64 {
	if seed != nil {
		rand.NewSource(*seed)
	} else {
		rand.NewSource(time.Now().UnixNano())
	}
	var cumulativeWeights []int64
	var sum int64 = 0
	for _, w := range weights {
		sum += w
		cumulativeWeights = append(cumulativeWeights, sum)
	}
	r := rand.Int63n(sum) + 1
	var i int
	for i = 0; i < len(cumulativeWeights); i++ {
		if r <= cumulativeWeights[i] {
			break
		}
	}
	return int64(i)
}
