package calc

// Compare a and b and return the smaller one
func MinUint8(a, b uint8) uint8 {
	if a < b {
		return a
	}
	return b
}

// Compare a and b and return the smaller one
func MinUint16(a, b uint16) uint16 {
	if a < b {
		return a
	}
	return b
}

// Compare a and b and return the smaller one
func MinUint32(a, b uint32) uint32 {
	if a < b {
		return a
	}
	return b
}

// Compare a and b and return the smaller one
func MinUint64(a, b uint64) uint64 {
	if a < b {
		return a
	}
	return b
}
