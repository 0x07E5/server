package calc

func IntToPtr(v int) *int {
	return &v
}

func Int8ToPtr(v int8) *int8 {
	return &v
}

func Int16ToPtr(v int16) *int16 {
	return &v
}

func Int32ToPtr(v int32) *int32 {
	return &v
}

func Int64ToPtr(v int64) *int64 {
	return &v
}

func UIntToPtr(v uint) *uint {
	return &v
}

func Uint8ToPtr(v uint8) *uint8 {
	return &v
}

func Uint16ToPtr(v uint16) *uint16 {
	return &v
}

func Uint32ToPtr(v uint32) *uint32 {
	return &v
}

func Uint64ToPtr(v uint64) *uint64 {
	return &v
}
