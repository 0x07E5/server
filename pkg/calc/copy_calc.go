package calc

import (
	"github.com/jinzhu/copier"
)

func Copy(dst interface{}, src interface{}) error {
	err := copier.Copy(dst, src)
	return err
}
