package calc

// Compare a and b and return the bigger one
func MaxUint8(a, b uint8) uint8 {
	if a > b {
		return a
	}
	return b
}

// Compare a and b and return the bigger one
func MaxUint16(a, b uint16) uint16 {
	if a > b {
		return a
	}
	return b
}

// Compare a and b and return the bigger one
func MaxUint32(a, b uint32) uint32 {
	if a > b {
		return a
	}
	return b
}

// Compare a and b and return the bigger one
func MaxUint64(a, b uint64) uint64 {
	if a > b {
		return a
	}
	return b
}

func MaxInt8(a, b int8) int8 {
	if a > b {
		return a
	}
	return b
}

// Compare a and b and return the bigger one
func MaxInt16(a, b int16) int16 {
	if a > b {
		return a
	}
	return b
}

// Compare a and b and return the bigger one
func MaxInt32(a, b int32) int32 {
	if a > b {
		return a
	}
	return b
}

// Compare a and b and return the bigger one
func MaxInt64(a, b int64) int64 {
	if a > b {
		return a
	}
	return b
}

// Compare a and b and return the bigger one
func MaxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}

// Compare a and b and return the bigger one
func MaxUInt(a, b uint) uint {
	if a > b {
		return a
	}
	return b
}
